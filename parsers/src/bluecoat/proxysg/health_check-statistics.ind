#! META
name: bluecoat-health_check-statistics
description: fetch health_check statistics
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: bluecoat
    "os.name": sgos

#! REMOTE::HTTP
url: "/health_check/statistics?stats_mode=0"
protocol: HTTPS

#! PARSER::AWK
BEGIN {
    memtags["name"] = "RAM"
}

# DNS Server
#  dns.8.8.8.8
#    Enabled  	OK  	UP
#    Last status: Success.
#    Successes (total): 63  	(last): Sat, 07 May 2016 16:37:44 GMT  	(consecutive): 63
#    Failures  (total): 7  	(last): Sat, 07 May 2016 16:27:03 GMT  	(consecutive): 0  	(external): 0
#    Last response time: 76 ms  	Average response time: 72 ms
#    Minimum response time: 65 ms  	Maximum response time: 88 ms
/dns/ {
	match($0, "dns.*$")
	dns_server=substr($0, RSTART+4, RLENGTH-4) # Get IP without "dns."
}
/Average response time/ {
	if (dns_server ~ ".{1,}") {
		match($0, "Average response time: [0-9]+ ")
		dnstags["dns-server"] = dns_server
		avgResponseTime = substr($0, RSTART+23, RLENGTH-24)

		writeDoubleMetricWithLiveConfig("dns-response-time", dnstags, "gauge", "60", avgResponseTime, "DNS Response Time (Average)", "number", "dns-server")
		# Clear the DNS server we remember so we don't accidently grab "average respones time" lines from other components
		dns_server=""
	}
}

