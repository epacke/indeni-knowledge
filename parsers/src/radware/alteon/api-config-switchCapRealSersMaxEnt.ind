#! META
name: api-config-switchCapRealSersMaxEnt
description: get the maximum number of real servers supported
type: monitoring
monitoring_interval: 59 minute 
requires:
    os.name: "alteon-os"
    vendor: radware
    or:
        -
            form.factor: vadc
        -
            form.factor: standalone

#! COMMENTS
real-servers-limit:
    why: |
        The alteon can handle a set number of real servers as part of its configuration. This is determined by the CUs allocated to the Alteon, whether predefined by the hardware, or allocation for the VM. Knowing when real server limit is nearing will be crucial for capacity planning
    how: |
        This script runs the "/config/switchCapRealSersCurrEnt" through the Alteon API gateway.
    without-indeni: |
        An administrator would need to log in to the device and run a CLI command or run the API command "/config/switchCapRealSersCurrEnt".
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Can be done through Management GUI (Vision or Alteon VX).

#! REMOTE::HTTP
url: /config/switchCapRealSersMaxEnt
protocol: HTTPS

#! PARSER::JSON
_metrics:
    -
        _value.double:
            _value: switchCapRealSersMaxEnt
        _tags:
            "im.name":
                _constant: "real-servers-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Real Servers Defined - Limit"
            "im.dstype.displayType":
                _constant: "number"
