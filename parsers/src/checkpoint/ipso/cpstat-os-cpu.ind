#! META
name: ipso-cpstat-os-cpu
description: fetch average and per-core CPU utilization from CHKP commands
type: monitoring
monitoring_interval: 1 minute
includes_resource_data: true
requires:
    "os.name": "ipso"

#! COMMENTS
cpu-usage:
    why: |
        High CPU could cause traffic to be dropped, and could result in a performance problem.
    how: |
        By running the Check Point "cpstat os -f multi_cpu" and "cpstat os -f cpu" commands, CPU utilization is retrieved for IP appliances.
    without-indeni: |
        An administrator could login and manually check CPU usage.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        CPU utilization is available in the vendor-provided tools.

#! REMOTE::SSH
${nice-path} -n 15 cpstat os -f cpu
${nice-path} -n 15 cpstat os -f multi_cpu

#! PARSER::AWK

# from the "cpu" flavor:
# CPU Usage (%):       0
/CPU Usage.*[0-9]/ {
    cputags["cpu-is-avg"] = "true"
    cputags["cpu-id"] = "all-average"
    cputags["resource-metric"] = "true"

    usage = $NF

    writeDoubleMetricWithLiveConfig("cpu-usage", cputags, "gauge", "60", usage, "CPU", "percentage", "cpu-id")  
}


# from the "multi_cpu" flavor:
# ---------------------------------------------------------------------------------
# |CPU#|User Time(%)|System Time(%)|Idle Time(%)|Usage(%)|Run queue|Interrupts/sec|
# ---------------------------------------------------------------------------------
# |   1|           1|             0|          98|       2|        2|            19|
# ---------------------------------------------------------------------------------
/CPU.*User Time/ {
    # Get rid of the spaces in the column names (like "User Time" 's space) because it
    # confuses the getColumns function
    colNames = trim($0)
    gsub(/ /, "", colNames)
    getColumns(colNames, "[ \t\|]+", columns)
}

/ [0-9].* [0-9].* [0-9]/ {
    cpuId = getColData(trim($0), columns, "CPU#")
    usage = getColData(trim($0), columns, "Usage(%)")

    cputags["cpu-id"] = cpuId
    cputags["cpu-is-avg"] = "false"
    cputags["resource-metric"] = "false"

    writeDoubleMetricWithLiveConfig("cpu-usage", cputags, "gauge", "60", usage, "CPU", "percentage", "cpu-id")
}
