#! META
name: chkp-clish-config-unsaved
description: Checks if a configuration made in clish is not saved
type: monitoring
monitoring_interval: 15 minutes
requires:
    vendor: "checkpoint"
    or:
        -
            os.name: "gaia"
        -
            os.name: "ipso"
    asg:
        neq: "true"

#! COMMENTS
config-unsaved:
    why: |
        When making a change in clish, the configuration is not written to disk until the command "save config" is run. This means that if the device reboots before the configuration is properly saved, it will be lost.
    how: |
        The clish command "show config-state" is used to retrieve the current save status.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the clish config save state is only available from the command line interface.

#! REMOTE::SSH
stty rows 80 ; ${nice-path} -n 15 clish -c 'show config-state' ; sleep 5 ; ${nice-path} -n 15 clish -c 'show config-state' ; ${nice-path} -n 15 dbget installer:last_sent_da_info ; ${nice-path} -n 15 grep "installer:last_sent_da_info" /config/active


#! PARSER::AWK

# DAClient os run on a schedule and updates the da info in memory. When this does not match the saved config the config state will go to "unsaved" even though the administrator has not changed it.
# To work around this the value of "last_sent_da_info" stored in memory and on disk will be compared, and if they do not match it will assume that the config is saved, even though the command returns "unsaved"
# stty rows 80 ; is needed to make the script work in IPSO

BEGIN {
	unsavedState = ""
	dbgetRevision = ""
	configRevision = ""
}

#saved
#unsaved
/^(saved|unsaved)$/ {
	# When checking the savestate the device could return "unsaved" now and then, when the savestate is really "saved"
	# The script will run twice, and if any of the two results are "saved" then that is the assumed state.
	
	# Count each state
	if ( $1 == "unsaved" ) {
		unsaved++
	} else if ($1 == "saved") {
		saved++
	}

	if (saved >= 1) {
		unsavedState = 0
	} else if (unsaved >= 1) {
		unsavedState = 1
	}
}

#1505042713
/^[0-9]+$/ {
	dbgetRevision = $1
}

#installer:last_sent_da_info 1505042713
/:last_sent_da_info/ {
	configRevision = $2
}

END {
	if (dbgetRevision != configRevision) {
		# If these revision are not a match, the configuration will show as unsaved even though it is saved.
		unsavedState = 0
	}

	if (unsavedState != "") {
		writeDoubleMetricWithLiveConfig("config-unsaved", null, "gauge", 900, unsavedState, "Configuration Unsaved?", "boolean", "")
	}
}