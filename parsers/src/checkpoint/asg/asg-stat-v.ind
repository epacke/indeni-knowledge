#! META
name: chkp-asg-stat-v
description: Retrieve status data
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: checkpoint
    asg: true

#! COMMENTS
blade-state:
    why: |
        A down blade in the security group can mean loss of redundancy and performance.
    how: |
        Indeni uses the built-in Check Point "asg stat -v" command to retrieve the current blade state.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the blade state is only available from the command line interface.

cluster-preemption-enabled:
    skip-documentation: true

cluster-member-active:
    skip-documentation: true

cluster-member-states:
    skip-documentation: true

cluster-state:
    cluster-member-active

#! REMOTE::SSH
${nice-path} -n 15 asg stat -v

#! PARSER::AWK

BEGIN {
	clusterState = 1
	tags["name"] = "ASG"
}

#| Chassis 1                     ACTIVE                                         |
/^\| Chassis/ {
	chassis = $3
}


#| 1  (local)     UP             Enforcing Security        10Feb17 19:37        |
#| 3              DOWN           Inactive                  NA                   |
/^\| [0-9].*(UP|DOWN)/ {

	# Remove (local) to not mess up counting columns
	gsub(/\(local\)/, "", $0)
	
	state = $3
	blade = $2

	# To catch a status with a space in between we need to split on two spaces or more.
	split($0, splitArr, /[ ]{2,}/)
	process = splitArr[3]
	
	# The blade needs to be up, and active, aka "Enforcing Security" to be considered ok.
	if (state == "UP" && process == "Enforcing Security") {
		bladeState = 1
	} else {
		bladeState = 0
		clusterState = 0
	}
	
	tags["name"] = "chassis: " chassis " blade: " blade
	
	writeDoubleMetricWithLiveConfig("blade-state", tags, "gauge", "300", bladeState, "Blade Status", "state", "name")
	
	# asg is always active if blade is ok
	writeDoubleMetric("cluster-member-active", tags, "gauge", "300", bladeState)
	
	iCluState++
	clusterMemberStates[iCluState, "state-description"] = process
	clusterMemberStates[iCluState, "name"] = "chassis: " chassis " blade: " blade
}



#| Chassis HA mode:               Active Up                                     |
#| Chassis Mode                | Active Up                                      |
/Chassis HA mode|Chassis Mode/ {
	haMode = $(NF-2) " " $(NF-1)
	if (haMode == "Active Up") {
		clusterPreempt = 0
	} else {
		clusterPreempt = 1
	}
	writeDoubleMetric("cluster-preemption-enabled", null, "gauge", 300, clusterPreempt)
}

END {
	writeDoubleMetricWithLiveConfig("cluster-state", tags, "gauge", "300", clusterState, "Cluster State", "state", "name")
	writeComplexMetricObjectArray("cluster-member-states", null, clusterMemberStates)
}