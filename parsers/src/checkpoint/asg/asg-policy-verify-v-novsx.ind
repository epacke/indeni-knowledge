#! META
name: chkp-asg-policy-verify-v-novsx
description: Check if policy is verified
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: checkpoint
    asg: true
    vsx:
        neq: true

#! COMMENTS
policy-verification-ok:
    why: |
        If the policy is not correctly installed on all the SGMs, some of them might not process traffic, which can mean loss of redundancy or capacity.
    how: |
        Indeni uses the built-in Check Point "asg policy verify -v" command to retrieve the policy status for all blades.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the policy install status is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 asg policy verify -v

#! PARSER::AWK

#|SGM    |Policy Name        |Policy Date    |Policy Signature |Status  |
/(Policy Name\s+ \|Policy Date\s+ \|Policy Signature)/ {
	# Parse the line into a column array.
	getColumns(trim($0), "[ ]+\\||^\\|", columns)
}

#|2_01   |-                  |-              |95c0bb5bc        |Failed  |
#|2_02   | TestPolicy        |10Feb17 19:37  |10c0bb5bc        |Success |
/\|[0-9]_[0-9][0-9]/ {
	# Use getColData to parse out the data for the specific column from the current line. The current line will be
	# split according to the same separator we've passed in the getColumns function (it's stored in the "columns" variable).
	# If the column cannot be found, the result of getColData is null (not "null").

	chassis = getColData(trim($0), columns, "SGM")
	status = getColData(trim($0), columns, "Status")
	
	split(chassis, chassisArr, "_")
	chassis = chassisArr[1]
	blade = chassisArr[2]
	
	if (status != "Success") {
		policyStatus = 0
	} else {
		policyStatus = 1
	}
	
	bladeTags["name"] = "chassis: " chassis " blade: " blade
	writeDoubleMetric("policy-verification-ok", bladeTags, "gauge", 300, policyStatus)
}