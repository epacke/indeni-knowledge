#! META
name: chkp-asg-version-verify
description: Make sure that system hardware components are running approved software and firmware versions.
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: checkpoint
    asg: true

#! COMMENTS
hardware-and-software-match:
    why: |
        Make sure that system hardware components are running approved software and firmware versions.
    how: |
        indeni uses the built-in Check Point "asg_version verify" command to retrieve the current compatibility.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the compatibility is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 asg_version verify

#! PARSER::AWK

BEGIN {
	hwSwMatch = 1
}


# SSM1 Firmware version: database and hardware do not match: database - 7.5.20, hardware - N/A
/database and hardware do not match/ {
	hwSwMatch = 0
}


END {
	writeDoubleMetric("chkp-hardware-and-software-match", null, "gauge", 300, hwSwMatch)
}