#! META
name: get-interesting-global-props
description: get certain global properties from the management db
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    role-management: true

#! COMMENTS
global-property-value:
    why: |
        Some global values are good to alert on, if they have a value which can cause issues.
    how: |
        By parsing the file $FWDIR/conf/objects_5_0.C on the management server, the setting can be retrieved.
    without-indeni: |
        An administrator could use SmartDashboard to view global properties, but would also need to know which values are optimal.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        An administrator could use SmartDashboard to view global properties, but would also need to know which values are optimal.

#! REMOTE::SSH
grep "http_max_concurrent_connections" $FWDIR/conf/objects_5_0.C

#! PARSER::AWK

# 			:http_max_concurrent_connections (1000)
/[0-9]/ {
	propname=$1
	gsub(/\:/, "", propname)
	paramtags["name"] = propname

	propvalue=$2
	gsub(/(\(|\))/, "", propvalue)

    writeDoubleMetricWithLiveConfig("global-property-value", paramtags, "gauge", "300", propvalue, "Global Properties", "number", "name")
}
