#! META
name: cphaprob_a_if_novsx
description: run "cphaprob -a if" on non-vsx
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    high-availability: "true"
    vsx:
        neq: true
    clusterxl: "true"
    role-firewall: true

#! COMMENTS
cphaprob-required-interfaces:
    why: |
        ClusterXL defines a certain number of interfaces which are required to be up for the cluster to be considered healthy. If there are less than these actually up, the cluster is not in a healthy state and traffic flow may be affected.
    how: |
        By using the Check Point built-in "cphaprob" command, the information is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing required interfaces is only available from the command line interface.

cphaprob-required-secured-interfaces:
    why: |
        ClusterXL defines a certain number of secured (sync) interfaces which are required to be up for the cluster to be considered healthy. If there are less than these actually up, the cluster is not in a healthy state and traffic flow may be affected.
    how: |
        By using the Check Point built-in "cphaprob" command, the information is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing required interfaces is only available from the command line interface.

cphaprob-up-interfaces:
    skip-documentation: true

cluster-vip:
    why: |
        This is the list of cluster virtual IP addresses also called floating IP adddresses for the cluster interfaces.
    how: |
        By using the Check Point built-in "cphaprob" command, the information is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing required interfaces is only available from the command line interface.

clusterxl-ccp-mode:
    why: |
        ClusterXL can operate in different modes, multicast or broadcast. All members of the same clusters should have the same setting to ensure redundancy works correctly.
    how: |
        By using the Check Point built-in "cphaprob" command, the information is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing required interfaces is only available from the command line interface.

cphaprob-up-secured-interfaces:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 cphaprob -a if

#! PARSER::AWK

BEGIN {
	foundInterfaces = 0
	foundSecuredInterfaces = 0
}

#HA module not started.
/HA module not started./ {
	servicesStarted = 0
}

#Required interfaces: 2
/Required interfaces/ {
	writeDoubleMetric("cphaprob-required-interfaces", t, "gauge", "60", $NF)
}

#Required secured interfaces: 1
/Required secured interfaces/ {
	writeDoubleMetric("cphaprob-required-secured-interfaces", t, "gauge", "60", $NF)
}

# Match UP secured and non-secured interfaces
# eth2       UP                    sync(secured), multicast
/UP.*secured/ {
    foundInterfaces = foundInterfaces + 1
}

# eth1            1.1.1.1
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/ {
	iint++
	clustervip[iint, $1]=$2
}

# eth2       UP                    sync(secured), multicast
/UP.*\(secured/ {
    foundSecuredInterfaces = foundSecuredInterfaces + 1
}

# eth5       UP                    non sync(non secured), multicast  (eth5.133  )
# eth1       UP                    non sync(non secured), multicast
/non sync/ {
	multicast=gsub(/multicast/,"")
	broadcast=gsub(/broadcast/,"")
	if ( multicast == 1) {
		syncMode = "multicast"
	} else if ( broadcast == 1) {
		syncMode = "broadcast"
	}
}

END {
	# Do not write metrics unless cluster services are started.
	if (servicesStarted != 0) {
		writeDoubleMetric("cphaprob-up-interfaces", t, "gauge", "60", foundInterfaces)
		writeDoubleMetric("cphaprob-up-secured-interfaces", t, "gauge", "60", foundSecuredInterfaces)
		writeComplexMetricObjectArray("cluster-vip", null, clustervip)
		writeComplexMetricStringWithLiveConfig("clusterxl-ccp-mode", null, syncMode, "CCP Mode")
	}
}
