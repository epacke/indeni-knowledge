#! META
name: chkp-cpstat_os_monitoring
description: get information from cpstat os
type: monitoring
monitoring_interval: 59 minute
requires:
    vendor: checkpoint

#! COMMENTS
model:
    skip-documentation: true

os-name:
    skip-documentation: true

vendor:
    skip-documentation: true

os-version:
    skip-documentation: true

serial-numbers:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 cpstat os ; ${nice-path} -n 15 asg stat

#! PARSER::AWK

############
# Why: Store information about OS, model etc in a more readable format.
# How: Use "cpstat".
###########

#Appliance SN:                  1319B00742
/^Appliance SN:/ {

	if ($3) {
		iSn++
		serialNumbers[iSn, "name"] = "hardware"
		serialNumbers[iSn, "serialnumber"] = trim($3)
		writeComplexMetricObjectArrayWithLiveConfig("serial-numbers", null, serialNumbers, "Serial Numbers")
	}
}

#SVN Foundation Version String: R77.30
/^SVN Foundation Version String:/ {
	writeComplexMetricString("os-version", null, $NF)
}

#OS Name:                       Gaia Embedded
#OS Name:                       Gaia
#OS Name:                       SecurePlatform Pro
#OS Name:                       SecurePlatform
/^OS Name:/ {
	writeComplexMetricStringWithLiveConfig("vendor", null, "Check Point", " Overview")
	
	split($0, splitArr, ":")
	osName = trim(splitArr[2])
}

#Appliance Name:                VMware Virtual Platform
#Appliance Name:                Check Point 2200
/^Appliance Name:/ {
	split($0, nameArr, ":")
	gsub(/Check Point/, "", nameArr[2])
	model = trim(nameArr[2])
}


#| System Status - 61000 |
#| VSX System Status - 61000 
/ System Status - / {
	asgModel = $(NF-1)
}


END {
	# If no model is found, then use the asgModel if it exists
	if (model == "" && asgModel != "") {
		model = asgModel
	}
	
	if (model == "") {
		model = "N/A"
	}


	writeComplexMetricStringWithLiveConfig("os-name", null, osName, " Overview")
	writeComplexMetricStringWithLiveConfig("model", null, model, " Overview")
}
