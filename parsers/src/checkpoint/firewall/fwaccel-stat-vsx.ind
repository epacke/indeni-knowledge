#! META
name: fw-accel-stat-vsx
description: get securexl status information
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    role-firewall: "true"
    vsx: true

#! COMMENTS
securexl-status:
    skip-documentation: true

securexl-disabled-from-rule:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l | grep VSID | awk '{print $NF}' | while read id; do vsenv $id && ${nice-path} -n 15 fw vsx stat $id && (${nice-path} -n 15 fwaccel stat); done

#! PARSER::AWK

function addVsTags(tags) {
	tags["vs.id"] = vsid
    tags["vs.name"] = vsname
}

BEGIN {
	vsid=""
	vsname=""
}

# VSID:            0
/VSID:/ {
	vsid = trim($NF)
}

# Name:            VSX-CXL2-Gear
/Name:/ {
	vsname = trim($NF)
}

# Accelerator Status : on
# Accelerator Status : no license for SecureXL
/Accelerator Status/ {
	addVsTags(vstags)
	if ( $5 == "license" ) {
		writeComplexMetricStringWithLiveConfig("securexl-status", vstags, "no-license", "SecureXL - State")	
	} else {
		writeComplexMetricStringWithLiveConfig("securexl-status", vstags, $NF, "SecureXL - State")
	}
}

#  disabled from rule #184
/disabled from rule/ {
	addVsTags(vstags)
    rule = $NF
    sub("#", "", rule)
    writeComplexMetricStringWithLiveConfig("securexl-disabled-from-rule", vstags, rule, "SecureXL - Templating Disabled From Rule Number")
}
