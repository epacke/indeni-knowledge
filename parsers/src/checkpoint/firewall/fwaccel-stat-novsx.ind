#! META
name: fw-accel-stat-novsx
description: get securexl status information
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    role-firewall: "true"

#! COMMENTS
securexl-status:
    why: |
        SecureXL is used to accelerate traffic. If it is disabled it could result in a reduction in the amount of throughput the device can handle. If used in a clustered environment, the user must ensure all members of the cluster have the same setting.
    how: |
        By using the Check Point built-in "fwaccel stat" command, the current status of SecureXL is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SecureXL status is only available from the command line interface.

securexl-disabled-from-rule:
    why: |
        SecureXL is used to accelerate traffic. If it is disabled it could result in a reduction in the amount of throughput the device can handle. Certain rules cause SecureXL to be disabled from that rule and downwards. If such a rule is placed in the very beginning of the rulebase, it would essentially disable SecureXL completely.
    how: |
        By using the Check Point built-in "fwaccel stat" command, the current status of SecureXL is retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing SecureXL status is only available from the command line interface.

#! REMOTE::SSH
${nice-path} -n 15 fwaccel stat

#! PARSER::AWK

# Accelerator Status : on
# Accelerator Status : no license for SecureXL
/Accelerator Status/ {
	if ( $5 == "license" ) {
		writeComplexMetricStringWithLiveConfig("securexl-status", null, "no-license", "SecureXL - State")	
	} else {
		writeComplexMetricStringWithLiveConfig("securexl-status", null, $NF, "SecureXL - State")
	}
}

#  disabled from rule #184
/disabled from rule/ {
    rule = $NF
    sub("#", "", rule)
    writeComplexMetricStringWithLiveConfig("securexl-disabled-from-rule", null, rule, "SecureXL - Templating Disabled From Rule Number")
}
