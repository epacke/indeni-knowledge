#! META
name: nexus-show-vrrp
description: Fetch the VRRP status
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: cisco
    os.name: nxos
    vrrp: true

#! COMMENTS
cluster-member-active:
    why: |
       Check if the device is the currently active VRRP (Virtual Router Redundancy Protocol) member. The active router is acting as the gateway for the VRRP group.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the VRRP state using the "show vrrp detail" command. The output includes a complete report of the VRRP state across all configure interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. State transitions generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

cluster-state:
    why: |
       Check if a configured VRRP (Virtual Router Redundancy Protocol) group has at least one active member. If no active members exist traffic would not be able to be routed. 
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the VRRP state using the "show vrrp detail" command. The output includes a complete report of the VRRP state across all configure interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. Only state transitions generate a syslog event. There is no explicit event for the last member failing.
    can-with-snmp: true
    can-with-syslog: false

cluster-preemption-enabled:
    why: |
       Check if a VRRP (Virtual Router Redundancy Protocol) group has preemption enabled. If preemption is enabled then a recovering device can trigger a switchover which may create a short interruption in traffic forwarding. It is recommended to disable preemption.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the VRRP state using the "show vrrp detail" command. The output includes a complete report of the VRRP state across all configure interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. Preemption events would generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

virtual-redundancy-groups:
    why: |
       Check if a VRRP (Virtual Router Redundancy Protocol) group are synchronized across a vPC cluster. It is expected that all VRRP groups would be the same across 2 vPC peers. The VRRP groups should use the same virtual IP, VLAN and group number.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the VRRP state using the "show vrrp detail" command. The output includes a complete report of the VRRP state across all configure interfaces.
    without-indeni: |
       Correlating all the groups across both vPC members can only be done manually, comparing the VRRP configuration.
    can-with-snmp: false
    can-with-syslog: false


#! REMOTE::SSH
show vrrp detail | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__
_metrics:
    -
        _groups:
            ${root}/TABLE_vrrp_group/ROW_vrrp_group:
                _temp:
                    state:
                        _text: "sh_group_state"
                    vlan_id:
                        _text: "sh_if_index"
                    grp_id:
                        _text: "sh_group_id"
                    vip_addr:
                        _text: "sh_vip_addr"
                _tags:
                    "im.name":
                        _constant: "cluster-member-active" 
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "VRRP - This Member State"
                    "im.identity-tags":
                        _constant: "name"  
                    "im.dstype.displayType":
                        _constant: "state"           
        _transform:
            _value.double:  |
                {
                    if (temp("state") == "Master") {print "1.0"} else {print "0.0"}
                }
            _tags:
                "name": |
                    { print "vrrp:" temp("vlan_id") ":" temp("grp_id") ":" temp("vip_addr") }
    -
        _groups:
            ${root}/TABLE_vrrp_group/ROW_vrrp_group:
                _temp:
                    state:
                        _text: "sh_group_state"
                    vlan_id:
                        _text: "sh_if_index"
                    grp_id:
                        _text: "sh_group_id"
                    vip_addr:
                        _text: "sh_vip_addr"
                _tags:
                    "im.name":
                        _constant: "cluster-state"
                    "group-vip":
                        _text: "sh_vip_addr" 
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "VRRP- Cluster State"
                    "im.identity-tags":
                        _constant: "name"
                    "im.dstype.displayType":
                        _constant: "state"
        _transform:
            _value.double:  |
                {
                    if (temp("state") == "Master" || temp("state") == "Backup")
                        { print "1.0" }
                    else
                        { print "0.0" }
                }
            _tags:
                "name": |
                    { print "vrrp:" temp("vlan_id") ":" temp("grp_id") ":" temp("vip_addr") }
    -
        _groups:
            ${root}/TABLE_vrrp_group/ROW_vrrp_group:
                _temp:
                    preempt:
                        _text: "sh_group_preempt"
                    vlan_id:
                        _text: "sh_if_index"
                    grp_id:
                        _text: "sh_group_id"
                    vip_addr:
                        _text: "sh_vip_addr"
                _tags:
                    "im.name":
                        _constant: "cluster-preemption-enabled" 
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "VRRP - Preemption"
                    "im.identity-tags":
                        _constant: "name"
                    "im.dstype.displayType":
                        _constant: "boolean"
        _transform:
            _value.double:  |
                {
                    if (temp("preempt") == "Enable") {print "1.0"} else {print "0.0"}
                }
            _tags:
                "name": |
                    { print "vrrp:" temp("vlan_id") ":" temp("grp_id") ":" temp("vip_addr") }
    -
        _groups:
            ${root}/TABLE_vrrp_group/ROW_vrrp_group:
                _temp:
                    state:
                        _text: "sh_group_state"
                    vlan_id:
                        _text: "sh_if_index"
                    grp_id:
                        _text: "sh_group_id"
                    vip_addr:
                        _text: "sh_vip_addr"
                _tags:
                    "im.name":
                        _constant: "virtual-redundancy-groups"
                    "name":
                        _constant: "vrrp"
                    "live-config":
                        _constant: "true"
                    "display-name":
                       _constant: "VRRP- Cluster Groups"
                    "im.identity-tags":
                        _constant: "name"
                    "im.dstype.displayType":
                        _constant: "state"
        _transform:
            _value.complex:  
                virtual-group: |
                    { print "vrrp:" temp("vlan_id") ":" temp("grp_id") ":" temp("vip_addr") }
        _value: complex-array
