#! META
name: nexus-show-processes-cpu
description: Nexus show processes cpu
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos
#! COMMENTS
process-cpu:
    why: |
       Capture the per-process CPU utilization. This information can be used to troubleshoot the root cause of overall system high cpu conditions.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show processes cpu" command. The output includes a table with all the processes and their respective CPU utilization.
    without-indeni: |
       It is possible to poll this data through SNMP but additional external logic would be required to analyze the information over time.
    can-with-snmp: false
    can-with-syslog: false

process-state:
    why: |
       Identify running state of critical system processes. If a critical process is reported as not running an alarm will be raised.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show processes cpu" command. The output includes a table with all the processes and their respective CPU utilization.
    without-indeni: |
       It is possible to poll this data through SNMP but additional external logic would be required to identify critical processes.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show processes cpu

#! PARSER::AWK

BEGIN {
    # List monitored processes
    processes["adjmgr"] = "0"
    processes["arp"] = "0"
    processes["bootvar"] = "0"
    processes["cisco"] = "0"
    processes["confcheck"] = "0"
    processes["core-dmon"] = "0"
    processes["dcos-xinetd"] = "0"
    processes["feature-mgr"] = "0"
    processes["fs-daemon"] = "0"
    processes["ifmgr"] = "0"
    processes["init"] = "0"
    processes["klogd"] = "0"
    processes["licmgr"] = "0"
    processes["pktmgr"] = "0"
    processes["rpc.statd"] = "0"
    processes["securityd"] = "0"
    processes["sysinfo"] = "0"
    processes["syslogd"] = "0"
    processes["sysmgr"] = "0"
    processes["ttyd"] = "0"
    processes["xinetd"] = "0"
}

# Option 1:
# 3927         1175      7458    157    0.0%  dcos-xinetd
#
# Option 2:
# CPU utilization for five seconds: 17%/3%; one minute: 18%; five minutes: 19%
#PID    Runtime(ms)  Invoked   uSecs  5Sec    1Min    5Min    TTY  Process
#-----  -----------  --------  -----  ------  ------  ------  ---  -----------
#    1        18510    369097      0   0.00%   0.00%  0.00%   -    init
# 3266            0         5      0   0.00%   0.00%  0.00%   -    portmap
/^\s*[0-9]+.*%/ {
    pid=$1
    #runtime=$2
    #invoked=$3
    #usecs=$4
    cpu=$5
    pname1=$6
    pname2=$9

    if (pname2 != "") {
        pname=pname2
    } else {
        pname=pname1
    }
    writeDebug("Found process " pname)

    sub(/%/, "", cpu)

    cputags["name"] = pid
    cputags["process-name"] = pname
    writeDoubleMetric("process-cpu", cputags, "gauge", 60, cpu)

    # Check monitored processes
    for (p in processes) {
        if (pname ~ p) {
            writeDebug("Found critical process " pname "(" p ")")
            processes[p] = "1"
            process_pid[p] = pid
        }
    }
}

END {
    for (pname in processes) {
        if (process_pid[pname]) {
            processtags["name"] = process_pid[pname]
        } else {
            processtags["name"] = "0" # Process is not running, so PID is unknown
        }
        processtags["process-name"] = pname
        processtags["description"] = "Process " pname " is required for normal system operation" # TODO
        writeDoubleMetric("process-state", processtags, "gauge", 300, processes[pname])
    }
}

