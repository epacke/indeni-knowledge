#! META
name: nexus-show-cdp-neighbors-detail
description: Nexus show cdp neighbors detail
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
known-devices:
    why: |
       Capture the neighboring devices' details. For each neighboring device the script will enumerate the device name and the IP addresses reported for the device. This information will be used to discover potential monitored devices. 
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the neighboring devices using the output of the "show cdp neighbors detail" command. The output includes device name for all neighboring devices. Each reported device lists its management IP address as well as any interface addresses.
       This metric only captures the management address and one connected address for each device.
       CDP (Cisco Discovery Protocol) is usually enabled by default on any Cisco device and periodically advertises the device's presence.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show cdp neighbors detail | xml

#! PARSER::XML
_omit:
    _bottom: 1
_vars:
    root: //__readonly__ 
_metrics:
    -
        _groups:
            ${root}/TABLE_cdp_neighbor_detail_info/ROW_cdp_neighbor_detail_info[v4mgmtaddr]:
                _temp:
                    name:
                        _text: "device_id"
                    mgmt_ip:
                        _text: "v4mgmtaddr"
                _tags:
                    "im.name":
                        _constant: "known-devices"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "CDP"
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.complex:
                name: |
                    {
                        if (temp("name") != "") {
                            print temp("name")
                        } else {
                            print "unknown"
                        }
                    }
                ip: |
                    {
                        if (temp("mgmt_ip") != "") {
                            print temp("mgmt_ip")
                        } else {
                            print "unknown"
                        }
                    }
        _value: complex-array            
    -
        _groups:
            ${root}/TABLE_cdp_neighbor_detail_info/ROW_cdp_neighbor_detail_info[v4addr]:
                _temp:
                    name:
                        _text: "device_id"
                    ip:
                        _text: "v4addr[1]"
                _tags:
                    "im.name":
                        _constant: "known-devices"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "CDP"
                    "im.identity-tags":
                        _constant: "name"
        _transform:
            _value.complex:
                name: |
                    {
                        if (temp("name") != "") {
                            print temp("name")
                        } else {
                            print "unknown"
                        }
                    }
                ip: |
                    {
                        if (temp("ip") != "") {
                            print temp("ip")
                        } else {
                            print "unknown"
                        }
                    }
        _value: complex-array            
