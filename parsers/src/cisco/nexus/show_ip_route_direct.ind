#! META
name: nexus-show-ip-route-direct
description: Nexus show ip route direct
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
connected-networks-table:
    why: |
       Capture the route entries that are directly connected to the device.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the output of the "show ip route direct" command. The output includes a table with the device's directly attached networks.
       The directly connected routes table has to match across vPC domain (cluster) memebers. If the tables are mismatched, an alert will be triggered.
    without-indeni: |
       It is possible to poll this data through SNMP but additional external logic would be required to correlate the connected routes table across vPC domain (cluster) members.
    can-with-snmp: false
    can-with-syslog: false
	
#! REMOTE::SSH
show ip route direct

#! PARSER::AWK

BEGIN {
    i = 0
}

#10.24.3.0/24, ubest/mbest: 1/0, attached
/^[0-9]+\..*\/[0-9]+,/ {
    split($1, r, ",")
    subnet = r[1]

    split(subnet, p, "/")
    prefix = p[1]
    mask = p[2]
    i++
    direct_routes[i, "network"] = trim(prefix)
    direct_routes[i, "mask"] = trim(mask)
}

END {
    writeComplexMetricObjectArrayWithLiveConfig("connected-networks-table", null, direct_routes, "Directly Connected Networks")
}
