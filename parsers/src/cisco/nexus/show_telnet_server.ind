#! META
name: nexus-telnet-server
description: Nexus show telnet server
type: monitoring
monitoring_interval: 59 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
telnet-enabled:
    why: |
        Capture whether Telnet is enabled on the device. Telnet sends information, including passwords, in clear text. It is highly recommeneded that it is disabled. If Telnet is detected to be enabled an alert would be triggered. 
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the current state of the telnet server by using the "show telnet server" command.
    without-indeni: |
       The administrator will have to manually log in to the device and check if Telnet is enabled. It is also possible to detect if TCP port 23 is open by using a port-scanning software.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show telnet server

#! PARSER::AWK
BEGIN {
    is_telnet_enabled = "n/a"
}

# telnet service enabled
/telnet service enabled/ {
    is_telnet_enabled = "true"
}

# telnet service not enabled
/telnet service not enabled/ {
    is_telnet_enabled = "false"
}

END {
    if (is_telnet_enabled != "n/a") {
        writeComplexMetricStringWithLiveConfig("telnet-enabled", null, is_telnet_enabled, "Telnet Enabled")
    }
}
