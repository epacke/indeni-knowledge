#! META
name: nexus-http-server
description: Nexus show http server
type: monitoring
monitoring_interval: 24 hours
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
http-server-enabled:
    why: |
        Capture whether the HTTP server is enabled on the device. HTTP sends information, including passwords, in clear text. It is highly recommeneded that it is disabled. If the HTTP server is detected to be enabled an alert would be triggered. 
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the current state of the telnet server by using the "show http-server" command.
    without-indeni: |
       The administrator will have to manually log in to the device and check if the HTTP server is enabled. It is also possible to detect TCP port 80 open by using a port-scanning software.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show http-server

#! PARSER::AWK

# dhcp-server enabled
/http-server enabled/ {
    writeComplexMetricStringWithLiveConfig("http-server-enabled", null, "true", "HTTP Server Enabled")
}

# http-server not enabled
/http-server not enabled/ {
    writeComplexMetricStringWithLiveConfig("http-server-enabled", null, "false", "HTTP Server Enabled")
}
