#! META
name: nexus-show-inventory
description: Nexus show inventory
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
serial-numbers:
    why: |
       Collect the inventory details of the device including all hardware components (chassis, linecards, power supplies etc.) and the serial numbers of each one of the components.
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the inventory data using the "show inventory" command.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::SSH
show inventory

#! PARSER::AWK

BEGIN {
    i = 0
}

# PID: N77-C7706           ,  VID: V01 ,  SN: FXS1123Q333
/PID:.*SN:/ {
    split($0, r, ",")

    split(r[1], p, ":")
    pid = p[2]
    split(r[3], s, ":")
    sn = s[2]

    if ((sn == "") || (sn ~ "N/A")) {
        next
    }

    i++
    sn_list[i, "name"] = trim(pid)
    sn_list[i, "serial-number"] = trim(sn)
}

END {
    writeComplexMetricObjectArrayWithLiveConfig("serial-numbers", null, sn_list, "Serial Numbers")
}
