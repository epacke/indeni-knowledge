#! META
name: ios-glbp-monitoring
description: Detemine system role in glbp configuration
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: ios 

#! COMMENTS
cluster-member-active:
    why: |
       Capture the device's use of the GLBP (Gateway Load Balancing Protocol) protocol. GLBP is a redundancy protocol that provides a way to used multiple routers as a 'single' default gateway. This increases the availability and reliability of routing paths via automatic default gateway selections. Static routes should be syncronized across GLBP members. If this is not the case, an alert should be generated. The active router is acting as the gateway for the GLBP group.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the GLBP state using the "show glbp" command. The output includes a complete report of the GLBP state across all configured interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. State transitions generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

cluster-state:
    why: |
       Check if a configured GLBP (Gateway Load Balancing Protocol) group has at least one active member. If no active members exist traffic would not be able to be routed.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the GLBP state using the "show glbp" command. The output includes a complete report of the GLBP state across all configured interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. Only state transitions generate a syslog event. There is no explicit event for the last member failing.
    can-with-snmp: true
    can-with-syslog: false

cluster-preemption-enabled:
    why: |
       Check if an GLBP (Gateway Load Balancing Protocol) group has preemption enabled. If preemption is enabled then a recovering device can trigger a switchover which may create a short interruption in traffic forwarding. It is recommended to disable preemption.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the GLBP state using the "show glbp" command. The output includes a complete report of the GLBP state across all configured interfaces.
    without-indeni: |
       It is possible to poll this data through SNMP. Preemption events would generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true
	
#! REMOTE::SSH
show glbp

#! PARSER::AWK

function resetVars () {
    interface = ""
    group = ""
    state = ""
    vip = ""
    mac = ""
    preempt = 0
    master = 0
}

BEGIN {
    # Initialize array indexes
    location = 0
    new_rec=0
    first_time=1
    metric_tags["live-config"] = "true"
}

# SAMPLE Input ##############################################
#
# FastEthernet1/0.1 - Group 31
#   State is Active
#   Virtual IP address is 172.16.31.254
#   Redirect time 600 sec, forwarder time-out 14400 sec
#   Preemption disabled
#   Active is local
#   Standby is 172.16.31.2, priority 190 (expires in 19.264 sec)
#   Priority 210 (configured)
#   Weighting 100 (default 100), thresholds: lower 1, upper 100
#   Load balancing: round-robin
# FastEthernet1/0.2 - Group 32
#   State is Standby
#   Virtual IP address is 172.16.32.254
#   Redirect time 600 sec, forwarder time-out 14400 sec
#   Preemption enabled, min delay 0 sec
#   Active is 172.16.32.1, priority 210 (expires in 6.912 sec)
#   Standby is local
#   Priority 190 (configured)
#   Weighting 100 (default 100), thresholds: lower 1, upper 100
#   Load balancing: round-robin
# FastEthernet1/0.3 - Group 33
#   State is Listen
#   Virtual IP address is 172.16.33.254
#   Redirect time 600 sec, forwarder time-out 14400 sec
#   Preemption enabled, min delay 0 sec
#   Active is 172.16.33.1, priority 210 (expires in 10.048 sec)
#   Standby is 172.16.33.2, priority 190 (expires in 16.512 sec)
#   Priority 170 (configured)
#   Weighting 100 (default 100), thresholds: lower 1, upper 100
#   Load balancing: round-robin


# Line for HA entry, search for interface type at start of line
# Ethernet # FastEthernet # Forty # Gigabit # Hundred # Serial # Ten
# FastEthernet1/0.1 - Group 31
/^[Ee]ther|^[Ff]ast|^[Ff]or|^[Gg]iga|^[Hh]un|^[Ss]er|^[Tt]en/ {
    new_rec = 1; forwarder = 0
    location++
    
    if (first_time = 1) {first_time=0} else {resetVars()}

    interface = trim($1)    
    split($0, parts, "-")
    split(trim(parts[2]), grp, " ")
    group = trim(grp[2])
    
    ha_entries[location, "interface"] = interface
    ha_entries[location, "group"] = group
}

/State is/ {
    # if in Forwarder section move to next record
    if (forwarder == 1) {next}
    state = trim($3)
    if (state == "Active") {ha_entries[location, "state"] = 1}
    if (state == "Standby") {ha_entries[location, "state"] = 0}
    if (state == "Listen") {ha_entries[location, "state"] = 0}
}

/Virtual IP/ {
    vip = trim($5)
    ha_entries[location, "vip"] = vip
}

/Virtual MAC/ {
    mac = trim($6)
    ha_entries[location, "mac"] = mac
}

/..[Pp]reemption/ {
    # if in Forwarder section move to next record
    if (forwarder == 1) {next}
    if (trim($2) ~ /enabled/) {preempt = 1} else {preempt = 0}
    ha_entries[location, "preempt"] = preempt
}

# needed to determine if master
/Active is/ {
    active_ip = trim($3)
}

# compare local with active 
/\) local/ {
    if (NF = 3 && trim($3) == "local") {local_ip = trim($2); gsub(/\(/, "", local_ip); gsub(/\)/, "", local_ip)}
    if (active_ip == "local") {ha_entries[location, "master"] = 1} else {ha_entries[location, "master"] = 0}
}

# flag to ignore entries in Forwarder sections
/Forwarder/{
    forwarder=1
}

END {
    for (i=1; i <= location; i++) {
        metric_tags["live-config"] = "true"
        metric_tags["name"] = "glbp:" ha_entries[i, "interface"] ":"  ha_entries[i, "group"] ":" ha_entries[i, "vip"]
        metric_tags["im.identity-tags"] = "name"

        # Member State
        metric_tags["display-name"] = "glbp - This Member State"
        metric_tags["im.dstype.displaytype"] = "state"
        writeDoubleMetric("cluster-member-active", metric_tags, "gauge", 1, ha_entries[i, "master"])

        # Cluster State
        # 1.0 if at least one member in the cluster is active and handling traffic OK, 0.0 otherwise. Must have a "name" tag.
        metric_tags["display-name"] = "glbp - Cluster State"
        metric_tags["im.dstype.displaytype"] = "state"
        writeDoubleMetric("cluster-state", metric_tags, "gauge", 1, ha_entries[i, "state"])

        # Preemption
        metric_tags["display-name"] = "glbp - Preemption"
        metric_tags["im.dstype.displaytype"] = "boolean"
        writeDoubleMetric("cluster-preemption-enabled", metric_tags, "gauge", 1, ha_entries[i, "preempt"])    
	delete metric_tags
    }
}

