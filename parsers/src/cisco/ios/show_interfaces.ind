#! META
name: ios-show-interface
description: IOS show interfaces
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: cisco
    os.name: ios

#! COMMENTS
network-interface-state:
    why: |
       Capture the interface state. If an interface transitions from up to down an alert would be raised.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP. Interface state transitions will generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

network-interface-admin-state:
    why: |
       Capture the interface administrative state.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP. Interface state transitions will generate a syslog event.
    can-with-snmp: true
    can-with-syslog: true

network-interface-speed:
    why: |
       Capture the interface speed in human readable format such as 1G, 10G, etc.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-duplex:
    why: |
       Capture the interface duplex in human readable format such as full or half. In modern network environments, it is uncommon to see interfaces configured to half-duplex. It is recommended to investigate as to whether this is an intentional configuration or an oversight. If unintentional, connectivity issues may arise.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP. If a duplex mismatch is detected on a port syslog messages will be generated.
    can-with-snmp: true
    can-with-syslog: true

network-interface-rx-frame:
    why: |
       Capture the interface Receive Errors (CRC) counter. If this counter increases an alarm will be raised.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-overruns:
    why: |
       Capture the interface Transmit Overrun errors counter.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-mac:
    why: |
       Capture the interface MAC address.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-hardware:
    why: |
       Capture the interface hardware, for example "DEC21140".
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-bytes:
    why: |
       Capture the interface Received Bytes counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-errors:
    why: |
       Capture the interface Received Errors counter. Packet loss may impact traffic performance.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-packets:
    why: |
       Capture the interface Received Packets counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-ipv4-address:
    why: |
       Capture the interface IPv4 address. Only relevant for layer 3 interfaces (and sub-interfaces), including Vlan interfaces (SVI).
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-ipv4-subnet:
    why: |
       Capture the interface IPv4 subnet mask. Only relevant for layer 3 interfaces, including Vlan interfaces (SVI).
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface #related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-carrier:
    why: |
       Capture the interface carrier state change counter. It would increase every time the interface changes state from up to down.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-mtu:
    why: |
       Capture the interface MTU (Maximum Transmit Unit).
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-bytes:
    why: |
       Capture the interface Transmitted Bytes counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-errors:
    why: |
       Capture the interface Transmit Errors counter. Transmission errors indicate an issue with duplex/speed matching.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-tx-packets:
    why: |
       Capture the interface Transmitted Packets counter. Knowing the amount of bytes and packets flowing through an interface can help estimate an interface's performance and utilization.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false

network-interface-rx-overruns:
    why: |
       Capture the interface Receive Overrun errors counter.
    how: |
       This script logs into the Cisco IOS device using SSH and retrieves the output of the "show interface" command. The output includes the interface related information and statistics.
    without-indeni: |
       It is possible to poll this data through SNMP.
    can-with-snmp: true
    can-with-syslog: false


#! REMOTE::SSH
show interfaces


#! PARSER::AWK
BEGIN {

    newnic=0
    firstloop=1
    entry=0

    metric_tags["live-config"] = "true"
    metric_tags["display-name"] = "Interfaces"
    metric_tags["im.identity-tags"] = "name"

}


## /^[Ee]|^[Ff]|^[Gg]|^[Hh]|^[Ss]|^[Tt]|^[Uu]/

# Interface 
/line protocol is/ {

    firstloop=firstloop
    if ( firstloop==0 ) { 
        newnic = 1
    } 
    position = NR
}



# SAMPLE Output (one interface)
#FastEthernet3/1 is administratively down, line protocol is down 
#  Hardware is i82543 (Livengood), address is ca01.3da1.0055 (bia ca01.3da1.0055)
#  Description: "*** My Description ***"
#  Internet address is 192.168.31.1/24
#  MTU 1500 bytes, BW 100000 Kbit/sec, DLY 100 usec, 
#     reliability 255/255, txload 1/255, rxload 1/255
#  Encapsulation ARPA, loopback not set
#  Keepalive set (10 sec)
#  Full-duplex, 100Mb/s, 100BaseTX/FX
#  ARP type: ARPA, ARP Timeout 04:00:00
#  Last input never, output never, output hang never
#  Last clearing of "show interface" counters never
#  Input queue: 0/75/0/0 (size/max/drops/flushes); Total output drops: 0
#  Queueing strategy: fifo
#  Output queue: 0/40 (size/max)
#  5 minute input rate 0 bits/sec, 0 packets/sec
#  5 minute output rate 0 bits/sec, 0 packets/sec
#     0 packets input, 0 bytes
#     Received 0 broadcasts (0 IP multicasts)
#     0 runts, 0 giants, 0 throttles 
#     0 input errors, 0 CRC, 0 frame, 0 overrun, 0 ignored
#     0 watchdog
#     0 input packets with dribble condition detected
#     0 packets output, 0 bytes, 0 underruns
#     0 output errors, 0 collisions, 0 interface resets
#     0 unknown protocol drops
#     0 babbles, 0 late collision, 0 deferred
#     0 lost carrier, 0 no carrier
#     0 output buffer failures, 0 output buffers swapped out


// {

    firstchar = substr($0, 1, 1)

    line = trim($0)
    split(line, field, " {1,}")


    if (newnic>0) {
        newnic=0
        }    
        
    if (firstchar = " " ) {firstloop=0}

    if ( line ~ /line protocol/ ) { 
        interface = trim(field[1])
        metric_tags["name"] = interface
        
        
        line_state = trim(field[NF])
        metric_tags["im.dstype.displayType"] = "state"
    
        if (line_state == "up") { val = "1.0" } else { val = "0.0" }
        writeDoubleMetric("network-interface-state", metric_tags, "gauge", 1, val)
        
        
        split($0, p1, ","); split(p1[1], p2, " ")
        admin_state = trim(field[NF])
        sub(/,/, "", admin_state)
        
        metric_tags["im.dstype.displayType"] = "state"
    
        if (admin_state == "up") { val = "1.0" } else { val = "0.0" }
        writeDoubleMetric("network-interface-admin-state", metric_tags, "gauge", 1, val)
        
    }

    if ( line ~ /[Hh]ardware is/ ) {  
        # Get HW for interface
        split($0, p1, ","); split(p1[1], p2, "[Hh]ardware is"); hardware = trim(p2[2]); sub(/,/, "", hardware) 
        # Get mac address
        split($0, p1, "address is "); split(p1[2], p2, " "); mac = trim(p2[1])
        
        metric_tags["im.dstype.displayType"] = ""
        writeComplexMetricString("network-interface-mac", metric_tags, mac)
    }

    if ( line ~ /nternet address/ ) { 
        split(trim(field[4]), p1, "/")
        ip_addr = trim(p1[1]) 
        ip_subnet=trim(p1[2]) 
                
        metric_tags["im.dstype.displayType"] = ""
        writeComplexMetricString("network-interface-ipv4-address", metric_tags, ip_addr)
        
        metric_tags["im.dstype.displayType"] = ""
        writeComplexMetricString("network-interface-ipv4-subnet", metric_tags, ip_subnet)
        
    }

    if ( line ~ /[Dd]escription:/ ) {
        desc = trim($0)
        sub(/[Dd]escription: /, "", desc)        
        gsub(/"/, "", desc)
        
        metric_tags["im.dstype.displayType"] = ""
        writeComplexMetricString("network-interface-description", metric_tags, desc)

    }

    if ( line ~ /MTU/ ) { mtu = trim(field[2]) 

        metric_tags["im.dstype.displayType"] = ""
        writeComplexMetricString("network-interface-mtu", metric_tags, mtu)
    
    }

    if ( line ~ /nput errors/ ) { 
        err_in = trim(field[1])
        
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-rx-errors", metric_tags, "counter", 1, err_in)
        
        crc = trim(field[4])

        frame_in = trim(field[6]) 
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-rx-frame", metric_tags, "counter", 1, frame_in)

        overruns = trim(field[8]) 
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-rx-overruns", metric_tags, "counter", 1, overruns)

    }
    
    if ( line ~ /utput errors/ ) { 
        err_out = trim(field[1])
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-tx-errors", metric_tags, "counter", 1, err_out)

        collisions = trim(field[4]) 
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-tx-collisions", metric_tags, "counter", 1, collisions)

    }
    
    if ( line ~ /ackets input/ ) { 
        pkt_in = trim(field[1])
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-rx-packets", metric_tags, "counter", 1, pkt_in)
        
        bytes_in = trim(field[4]) 
        metric_tags["im.dstype.displayType"] = "bytes"
        writeDoubleMetric("network-interface-rx-bytes", metric_tags, "counter", 1, bytes_in)
    }
    
    if ( line ~ /ackets output/ ) { 
        pkt_out = trim(field[1])
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-tx-packets", metric_tags, "counter", 1, pkt_out)        
        
        bytes_out = trim(field[4])
        metric_tags["im.dstype.displayType"] = "bytes"
        writeDoubleMetric("network-interface-tx-bytes", metric_tags, "counter", 1, bytes_out)
        
        underruns = trim(field[6]) 
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-tx-underruns", metric_tags, "counter", 1, underruns)        
    }

    if ( line ~ /carrier/ ) { 
        lost_out = trim(field[1])
        metric_tags["im.dstype.displayType"] = "number"
        writeDoubleMetric("network-interface-tx-carrier", metric_tags, "counter", 1, lost_out)
        
        no_out = trim(field[4])
    }
    
    if ( line ~ /[D|d]uplex/ ) { 
        # Extract duplex
        duplex = trim(field[1]); 
        gsub(/,/, "", duplex); 
        #sub(/-[D|d]uplex/, "", duplex); 
        
        metric_tags["im.dstype.displayType"] = ""
        writeComplexMetricString("network-interface-duplex", metric_tags, duplex)
        
        # Extract speed
        split(line, sp, ",");  
        speed = trim(sp[2]); 
        gsub(/,/, "", speed) 
        
        metric_tags["im.dstype.displayType"] = ""
        
        if (interface ~ /[Ff]ast/) { speed = "100M"}
        if (interface ~ /[Gg]iga/) { speed = "1G"}
        if (interface ~ /[Tt]en/) { speed = "10G"}
        if (interface ~ /[Ff]or/) { speed = "40G"}
        if (interface ~ /[Hh]un/) { speed = "100G"}
        
        writeComplexMetricString("network-interface-speed", metric_tags, speed)
        
    }
}

END {
}

