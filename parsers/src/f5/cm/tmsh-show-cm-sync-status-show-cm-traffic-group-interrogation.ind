#! META
name: f5-show-cm-sync-status-show-cm-traffic-group-interrogation
description: Determine if the device is part of a cluster and generate its cluster id
type: interrogation

requires:
    vendor: "f5"
    product: "load-balancer"
    shell: "bash"

#! REMOTE::SSH
tmsh -q -c "show cm sync-status; show cm traffic-group"

#! PARSER::AWK

BEGIN {
	highAvailability = 0
	clusterId = ""
    iDevice = 0
}

#Mode     high-availability
/^Mode/{
	if($2 == "high-availability"){
		writeTag("high-availability", "true")
		highAvailability = 1
	}
}

#traffic-group-1           mydevice.domain.local  active   false
/[^\s]+\s+[^\s]+\s+[^\s]+\s+(true|false)$/{
	
	#Make sure that we have not added this device before
	found = 0
    for(i in addedDevicesDictionary){
        if(addedDevicesDictionary[i] == $2){
            found = 1
        }
    }
    
    if(found == 0){
    	#If the device was not found already. Add the device to the addedDevicesDictionary
        iDevice++
		addedDevicesDictionary[iDevice] = $2
    }
}

END {
	if(highAvailability == 1){
        
        #Bubble sort to make sure that the id is the same across all devices no matter order of appearance
        do {
            haschanged = 0
            for(i in addedDevicesDictionary) {
                if ( addedDevicesDictionary[i] > addedDevicesDictionary[i+1] && addedDevicesDictionary[i+1] != "" ) {
                    device = addedDevicesDictionary[i]
                    addedDevicesDictionary[i] = addedDevicesDictionary[i+1]
                    addedDevicesDictionary[i+1] = device
                    haschanged = 1
                }
            }
        } while ( haschanged == 1 )
        
        #Concatenate the entries
        clusterId = ""
        for(i in addedDevicesDictionary){
            clusterId = clusterId addedDevicesDictionary[i]
        }
        
        #If not empty, write the tag
        if(clusterId != ""){
            writeTag("cluster-id", clusterId)
        }
	}
}
