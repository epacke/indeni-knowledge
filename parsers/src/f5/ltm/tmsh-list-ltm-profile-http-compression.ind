 #! META
name: f5-tmsh-list-ltm-profile-http-compression
description: Find use content-types that is already compressed
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
f5-compression-profile-precompressed-content-types:
    why: |
        Using the F5 device to compress content is a way of accelerating application content delivery. However, compressing content that is already pre-compressed results in longer response times to clients and uses system resources in vain. This metric retrieves profiles with pre-compressed content.
    how: |
        This alert logs into the F5 through SSH and looks through all compression profiles for pre-compressed content-types.
    without-indeni: |
        Log into the device through SSH. Enter TMSH and issue the command "cd /;list ltm profile http-compression content-type-include". Look through each entry in the list and look for pre-compressed content-types.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
${nice-path} -n 15 tmsh -q -c "cd /;list ltm profile http-compression recursive content-type-include"
 
#! PARSER::AWK

BEGIN {

    precompressedContentTypeArray["video"] = /.*video.*/
    precompressedContentTypeArray["audio"] = /.*audio.*/
    precompressedContentTypeArray["x-tar"] = /.*x-tar.*/
    precompressedContentTypeArray["zip"] = /.*zip.*/
    precompressedContentTypeArray["compress"] = /.*compress.*/
    precompressedContentTypeArray["jpeg"] = /.*jpeg.*/
    precompressedContentTypeArray["x-m4v"] = /.x-m4v.*/
    precompressedContentTypeArray["ogg"] = /.*\/ogg.*/
    precompressedContentTypeArray["png"] = /.*png.*/
    precompressedContentTypeArray["gif"] = /.*\/gif\".*/

}

#ltm profile http-compression
/^ltm profile http-compression/ {
    
    delete compressionArray
    compressionTags["name"] = "/" $4

}

#    content-type-include {
/^\s+content-type-include \{/ {

    for(description in precompressedContentTypeArray){
        if(match($0, precompressedContentTypeArray[description])) {
            iCompr++
            compressionArray[iCompr, "filetype"] = description
        }
    }
    
}

#}
/^\}$/ {
    
    writeComplexMetricObjectArray("f5-compression-profile-precompressed-content-types", compressionTags, compressionArray)

}
