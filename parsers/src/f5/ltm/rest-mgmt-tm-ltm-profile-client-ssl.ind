 #! META
name: f5-rest-mgmt-tm-ltm-profile-client-ssl
description: Determine if an ssl profile is vulnerable to ticket bleed or not
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
ssl-weak-impl:
    why: |
        Ticketbleed is a vulnerability on F5 products that enables the attacker to extract up to 31 bytes of uninitialized memory at a time. The memory leak may contain sensitive data or even key material.
    how: |
        This alert uses the iControl REST interface to determine which SSL Client profiles that are using "session tickets".
    without-indeni: |
        An adminstrator would have to login to the device through SSH, execute the command "tmsh -q -c 'cd /; list ltm profile client-ssl one-line recursive'". The output would then have to be parsed to determine if any of the client ssl profiles has "session-ticket" set to "enabled".
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/version
protocol: HTTPS

#! PARSER::JSON

_dynamic_vars:
    _temp:
        "edition":
            _value: "$.entries.*.nestedStats.entries.Edition.description"
        "version":
            _value: "$.entries.*.nestedStats.entries.Version.description"
    _transform:
        _dynamic:
            "versionEdition": |
                {
                    edition = temp("edition")
                    
                    if(match(edition, /^.*\sHF/)) {
                        sub(/^.*\sHF/, "", edition)
                    } else {
                        #Assume edition 0. Version 0 is called Final, and this would also catch other strange editions
                        edition = 0
                    }
                    print temp("version") "_" edition
                }

#! REMOTE::HTTP
url: /mgmt/tm/ltm/profile/client-ssl
protocol: HTTPS

#! PARSER::JSON
#https://support.f5.com/csp/article/K05121675

_metrics:
    - #Profiles where tickets are not enabled
        _groups:
            "$.items[0:][?(@.sessionTicket == 'enabled')]":
                _tags:
                    "im.name":
                        _constant: "ssl-weak-impl"
                    "im.dstype.displaytype":
                        _constant: "boolean"
                    "name":
                        _constant: "ticketbleed"
                    "profile-name":
                        _value: "fullPath"
                    "type":
                        _constant: "Client SSL Profile"
        _transform:
            _value.complex:
                value: |
                    {
                        # This array shows the latest vulnerable version
                        # We will compare the versionEdition with this 
                        # array to know if the device version is later or not.
                        vulnerableVersions["11.5.0"] = 7
                        vulnerableVersions["11.5.1"] = 11
                        vulnerableVersions["11.5.2"] = 1
                        vulnerableVersions["11.5.3"] = 2
                        vulnerableVersions["11.5.4"] = 2
                        vulnerableVersions["11.6.0"] = 8
                        vulnerableVersions["11.6.1"] = 1
                        vulnerableVersions["12.0.0"] = 4
                        vulnerableVersions["12.1.0"] = 2
                        vulnerableVersions["12.1.1"] = 2
                        vulnerableVersions["12.1.2"] = 0
                        
                        split(dynamic("versionEdition"), versionEditionArr, /_/)
                        
                        version = versionEditionArr[1]
                        edition = versionEditionArr[2]
                        
                        if(version in vulnerableVersions){
                            
                            if(vulnerableVersions[version] >= edition){
                                print "true"
                            } else {
                                print "false"
                            }
                        } else {
                            print "false"
                        }
                    }
    - #Profiles where tickets are not enabled, and thus not vulnerable.
        _groups:
            "$.items[0:][?(@.sessionTicket != 'enabled')]":
                _tags:
                    "im.name":
                        _constant: "ssl-weak-impl"
                    "im.dstype.displaytype":
                        _constant: "boolean"
                    "name":
                        _constant: "ticketbleed"
                    "profile-name":
                        _value: "fullPath"
                    "type":
                        _constant: "Client SSL Profile"
        _transform:
            _value.complex:
                value: |
                    {
                        print "false"
                    }
