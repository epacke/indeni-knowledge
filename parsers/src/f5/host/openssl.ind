#! META
name: f5-openssl
description: Determines if the default managment certificate is used or not
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
default-management-certificate-used:
    why: |
        Using the default management certificate could enable a potential attacker to perform a man-in-the-middle attack without administrators knowing it. This indeni alert checks if the default management certificate is used.
    how: |
        This indeni script logs into the device through SSH and executes the command "openssl x509 -in /etc/httpd/conf/ssl.crt/server.crt -text -noout".
    without-indeni: |
        An administrator can verify if the default management certificate is used by logging into the device via the web interface, clicking on "System" -> "Device Certficates". If "Certificate subject(s)" contains "localhost" the default certificate is used. While performing this check it would also be prudent to check if the certificate used in trusted by looking at the address bar of the browser.
    can-with-snmp: false
    can-with-syslog: false

    
#! REMOTE::SSH
openssl x509 -in /etc/httpd/conf/ssl.crt/server.crt -text -noout

#! PARSER::AWK

#        Issuer: C=--, ST=WA, L=Seattle, O=MyCompany, OU=MyOrg, CN=localhost.localdomain/emailAddress=root@localhost.localdomain
/^\s+Issuer:/{

    if(match($0, /.+?CN=.*localhost.*$/)){
        writeComplexMetricString("default-management-certificate-used", null, "true")
    } else {
        writeComplexMetricString("default-management-certificate-used", null, "false")
    }
    
}
