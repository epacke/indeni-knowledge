#! META
name: f5-tmsh-show-sys-performance-throughput-raw
description: Get current SSL TPS
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
license-elements-used:
    why: |
        Depending on platform, license SSL Transactions Per Seconds (TPS), or system throughput could be limited. This metric will track and alert when the usage is nearing, or has reached the limit.
    how: |
        This alert uses the F5 iControl REST API to retrieve current TPS and throughput of the unit.
    without-indeni: |
        An administrator could could periodically log into the device through SSH, enter TMSH and execute the command "show sys performance throughput detail raw" to get this data.
    can-with-snmp: false
    can-with-syslog: false
    
#! REMOTE::SSH
tmsh -q show sys performance throughput detail raw

#! PARSER::AWK

#Client Bits In                        132557004  129647133                     183154812
/Client Bits In/{
    clientBitsIn = $4
}

#Client Bits Out                       603210992  488761174                     876960406
/Client Bits Out/{
    clientBitsOut = $4
}

#Server Bits In                        133686411  130672797                     184317636
/Server Bits In/{
    serverBitsIn = $4
}

#Server Bits Out                       595569427  487756939                     871842236
/Server Bits Out/{
    serverBitsOut = $4
}


#SSL TPS                              546        512                           596
/^SSL TPS/ {
    
    section="ssltransactions"
    
	licenseElementsTag["name"] = "SSL TPS"
	writeDoubleMetricWithLiveConfig("license-elements-used", licenseElementsTag, "gauge", 300, $3, "License Utilization", "number", "name")
}

END {
    
    # License limits are enforced by the larger of the two values measured from
    # combining Client Bits In plus Server Bits Out or combining Client Bits Out
    # plus Server Bits In.
    
    clientToServerSide = clientBitsIn + serverBitsOut
    serverToClientSide = clientBitsOut + serverBitsIn
    
    licenseElementsTag["name"] = "VE Edition Maximum Throughput (Mbps)"
    
    if(clientToServerSide>serverToClientSide){
        usageValue = clientToServerSide/1000000
    } else {
        usageValue = serverToClientSide/1000000
    }
    
    writeDoubleMetricWithLiveConfig("license-elements-used", licenseElementsTag, "gauge", 300, usageValue, "License Utilization", "number", "name")
    
}
