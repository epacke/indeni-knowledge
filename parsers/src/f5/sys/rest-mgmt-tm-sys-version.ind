#! META
name: f5-rest-mgmt-tm-sys-version
description: Determine end of software support
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
software-eos-date:
    why: |
        Ensuring the software being used is always within the vendor's list of supported versions is critical. Otherwise, during a critical issue, the vendor may decline to provide technical support. F5 Networks posts the list of supported software on their website (https://support.f5.com/csp/article/K5903). indeni tracks that list and updates this script to match.
    how: |
        This script uses the F5 iControl REST API to retrieve the current software version (the equivalent of running "show sys version" in TMSH) and based on the software version and the F5 Networks provided information at https://support.f5.com/csp/article/K5903 the correct end of support date is used.
    without-indeni: |
        Manual tracking by an administrator is usually the only method for knowing when a given device may be nearing its software end of support and is in need of upgrading.
    can-with-snmp: false
    can-with-syslog: false
os-name:
    why: |
       Capture the device operating system name.
    how: |
       This script uses the F5 iControl REST API to retrieve the name of the OS.
    without-indeni: |
       An administrator could extract this data by logging in to the device, entering TMSH and issuing the command "show sys version".
    can-with-snmp: true
    can-with-syslog: false
os-version:
    why: |
       Capture the device operating system version.
    how: |
        This script uses the F5 iControl REST API to retrieve the version of the OS.
    without-indeni: |
       An administrator could extract this data by logging in to the device, entering TMSH and issuing the command "show sys version".
    can-with-snmp: true
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/version?$select=Version
protocol: HTTPS

#! PARSER::JSON

_metrics:
    -
        _tags:
            "im.name":
                _constant: "software-eos-date"
            "live-config":
               _constant: "true"
            "display-name":
                _constant: "Software End of Support"
            "im.dstype.displayType":
                _constant: "date"
        _temp:
            "version":
                _value: "$.entries.*.nestedStats.entries.Version[?(@.description in ['12.1.0','12.1.1','12.1.2','12.1.3','12.1.4','12.1.5','12.1.6','12.1.7','12.1.8','12.1.9','12.0.0','11.6.0','11.6.1','11.6.2','11.6.3','11.6.4','11.6.5','11.6.6','11.6.7','11.6.8','11.6.9','11.5.4','11.5.3','11.5.2','11.6.0','11.5.1','11.5.0','11.4.1'])].description"
        _transform:
            _value.double: |
                {
                    #Any version in this list must also exist in the json path array above.
                    endofSoftwareTechnicalSupport["12.1.0"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.1"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.2"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.3"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.4"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.5"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.6"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.7"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.8"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.1.9"] = "2022-05-18"
                    endofSoftwareTechnicalSupport["12.0.0"] = "2017-12-02"
                    endofSoftwareTechnicalSupport["11.6.0"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.1"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.2"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.3"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.4"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.5"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.6"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.7"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.8"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.6.9"] = "2022-05-10"
                    endofSoftwareTechnicalSupport["11.5.4"] = "2020-04-08"

                    version = temp("version")

                    split(endofSoftwareTechnicalSupport[version], dateArr, /-/)
                    secondsSinceEpoch = date(dateArr[1], dateArr[2], dateArr[3])

                    print secondsSinceEpoch
                }
    -
        _tags:
            "im.name":
                _constant: "vendor"
        _value.complex:
            value:
                _constant: "F5"
    -
        _tags:
            "im.name":
                _constant: "os-name"
        _value.complex:
            value:
                _constant: "BIG-IP"
    -
        _tags:
            "im.name":
                _constant: "os-version"
        _temp:
            "version":
                _value: "$.entries.*.nestedStats.entries.Version.description"
        _transform:
            _value.complex:
                value: |
                    {
                        print temp("version")
                    }
