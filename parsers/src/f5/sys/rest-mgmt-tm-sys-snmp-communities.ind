 #! META
name: f5-rest-mgmt-tm-sys-snmp-communities
description: Determine if any SNMP communities for SNMPv1 or SNMPv2 has been configured
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
unencrypted-snmp-configured:
    why: |
        Version 1 and 2 of the SNMP protocol is unencrypted. This could potentially allow an attacker to obtain valuable information about the infrastructure.
    how: |
        This alert uses the iControl REST interface to extract SNMP configuration.
    without-indeni: |
        Login to the device's web interface and click on "System" -> "SNMP" -> "Agent" -> " Access (v1, v2c)". This would show a list of configured access for SNMP version 1 and 2c.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/snmp/communities
protocol: HTTPS

#! PARSER::JSON

_metrics:
    - # Record community configuration
        
        _tags:
            "im.name":
                _constant: "unencrypted-snmp-configured"
            "im.dstype.displaytype":
                _constant: "boolean"
        _temp:
            "noCommunities":
                #'default' means localhost and is useful to keep in case of local SNMP troubleshooting
                _count: "$.items[0:][?(@.source != 'default' && @.source != '127.0.0.1')]"
        _transform:
            _value.complex:
                value: |
                    {                        
                        if(temp("noCommunities") > 0){
                            print "true"
                        } else {
                            print "false"
                        }
                    }
