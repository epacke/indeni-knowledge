#! META
name: f5-tmsh-list-sys-syslog-test-tcp
description: Test configured tcp syslog servers
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
tcp-syslog-state:
    why: |
        A syslog server is not reachable from the device. This could result in lost messages in which traceability would be severely impacted. This script verifies that any configured syslog servers using TCP is can be reached by establishing a TCP connection to each one.
    how: |
        This alert logs into the F5 device through SSH, parses the output of the command "tmsh list sys syslog", extracts the configured tcp syslog servers and test the connection to them.
    without-indeni: |
        An administrator could could periodically log into the device through SSH, enter TMSH and execute the command "list sys syslog" in order to identify the configured syslog servers. For each syslog server using TCP he could then test the connectivity by issuing the command "nc -v -z <ip> <port>".
    can-with-snmp: false
    can-with-syslog: false
    
#! REMOTE::SSH

tmsh -q -c "list sys syslog" | grep -Po '(?<=tcp\(\\")[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(.+?)port\s+\([0-9]+\)' |  while read line ; do nccmd="nc -v -w 1 -z $(echo $line | sed -e 's/" port//g' -e 's/[)(]//g')"; echo $nccmd;  done | (while read cmd; do { ($cmd) & sleep 0.1 ; }  done ; sleep 10;) 

#! PARSER::AWK

#Explanation of the command
#1. First get the output of list sys sylog
#2. Send it to grep which would return "10.0.0.10\" port (514)"
#3. Remove '\" port (' and ')"' using sed
#4. Test the syslog server using netcat

{
    writeDebug($0)
}

#nc: connect to 10.0.0.10 port 514 (tcp) timed out: Operation now in progress
/timed out/{

    ip = $4
    port = $6
    ipPort = ip ":" port
    
    syslogTags["name"] = ipPort
    
    writeDoubleMetric("tcp-syslog-state", syslogTags, "gauge", 600, 0)
    
}

#nc: connect to 192.168.197.16 port 95 (tcp) failed: Connection refused
/Connection refused/{

    ip = $4
    port = $6
    ipPort = ip ":" port
    
    syslogTags["name"] = ipPort
    
    writeDoubleMetric("tcp-syslog-state", syslogTags, "gauge", 600, 0)
    
}

#Connection to 192.168.197.16 80 port [tcp/http] succeeded!
/succeeded!/{

    ip = $3
    port = $4
    ipPort = ip ":" port
    
    syslogTags["name"] = ipPort
    
    writeDoubleMetric("tcp-syslog-state", syslogTags, "gauge", 600, 1)
}
