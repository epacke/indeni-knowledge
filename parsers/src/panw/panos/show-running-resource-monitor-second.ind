#! META
name: panos-show-running-resource-monitor-second
description: get the dataplane cpu stats
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
cpu-usage:
    why: |
        Tracking the CPU utilization of all processing cores on a firewall is critical to ensure capacity isn't reached. Should capacity be reached on data plane cores, new connections and sessions may fail to traverse through the firewall. A high CPU utilization on the management plane may result in issues in managing the device, or updating its settings.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current status of all of the data plane's and management plane's CPUs.
    without-indeni: |
        The utilization of the data plane and management plane CPUs is available through SNMP (see https://live.paloaltonetworks.com/t5/Management-Articles/SNMP-for-Monitoring-Palo-Alto-Networks-Devices/ta-p/61052 ).
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><running><resource-monitor><second><last>1<%2Flast><%2Fsecond><%2Fresource-monitor><%2Frunning><%2Fshow>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result/resource-monitor
_metrics:
    -
        _groups:
            ${root}/data-processors/*/second/cpu-load-average/entry:
                _value.double:
                    _text: value
                _temp:
                    coreid:
                        _text: "coreid"
                    dpid:
                        _name: ancestor::node()[3]
                _tags:
                    "im.name":
                        _constant: "cpu-usage"
                    "live-config":
                       _constant: "true"
                    "display-name":
                        _constant: "CPU Usage"
                    "im.dstype.displayType":
                        _constant: "percentage"
                    "im.identity-tags":
                        _constant: "cpu-id"
                    "cpu-is-avg":
                        _constant: "false"
        _transform:
            _tags:
                "cpu-id": |
                    {
                        print temp("dpid") ": " temp("coreid")
                    }
