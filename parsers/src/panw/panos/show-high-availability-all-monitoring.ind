#! META
name: panos-show-high-availability-all-monitoring
description: track health of HA
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    "high-availability": true

#! COMMENTS
cluster-member-active:
    why: |
        Tracking the state of a cluster member is important. If a cluster member which used to be the active member of the cluster no longer is, it may be the result of an issue. In some cases, it is due to maintenance work (and so was anticipated), but in others it may be due to a failure in the firewall or another component in the network.
    how: |
        This script uses the Palo Alto Networks API to retrieve the status of the high availability function of the firewall and specifically retrieves the local member's state.
    without-indeni: |
        The status of high availability is visible in the web interface, as a widget on the main screen.
    can-with-snmp: true
    can-with-syslog: true
cluster-state:
    why: |
        Tracking the state of a cluster is important. If a cluster which used to be healthy no longer is, it may be the result of an issue. In some cases, it is due to maintenance work (and so was anticipated), but in others it may be due to a failure in the members of the cluster or another component in the network.
    how: |
        This script uses the Palo Alto Networks API to retrieve the status of the high availability function of the cluster and specifically retrieves the local member's and peer's states.
    without-indeni: |
        The status of high availability is visible in the web interface, as a widget on the main screen.
    can-with-snmp: true
    can-with-syslog: true
cluster-preemption-enabled:
    why: |
        Preemption is a function in clustering which sets a primary member of the cluster to always strive to be the active member. The trouble with this is that if the active member that is set with preemption on has a critical failure and reboots, the cluster will fail over to the secondary and then immediately fail over back to the primary when it completes the reboot. This can result in another crash and the process would happen again and again in a loop. The Palo Alto Networks firewalls have a means of dealing with this ( https://live.paloaltonetworks.com/t5/Learning-Articles/Understanding-Preemption-with-the-Configured-Device-Priority-in/ta-p/53398 ) but it is generally a good idea not to have the preemption feature enabled.
    how: |
        This script uses the Palo Alto Networks API to retrieve the status of the high availability function of this cluster member and specifically the preemption setting.
    without-indeni: |
        Going into a preemption loop is difficult to detect. Normally an administrator will notice service disruption. Then through manual inspection the administrator will determine there is a preemption loop.
    can-with-snmp: true
    can-with-syslog: true
cluster-config-synced:
    why: |
        Normally two Palo Alto Networks firewalls in a cluster work together to ensure their configurations are synchronized. Sometimes, due to connectivity or other issues, the configuration sync may be lost. In the event of a fail over, the secondary member will take over but will be running with a different configuration compared to the primary (the original active member). This can result in service disruption.
    how: |
        This script uses the Palo Alto Networks API to retrieve the status of the high availability function of this cluster and specifically the status of the config synchronization.
    without-indeni: |
        The status of configuration sync is visible in the web interface, as a widget on the main screen.
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><high-availability><all></all></high-availability></show>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _temp:
            state:
                _text: "${root}/group/local-info/state"
        _tags:
            "im.name":
                _constant: "cluster-member-active"
            "name":
                _constant: "Firewall Clustering"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Cluster Member State (this)"
            "im.dstype.displayType":
                _constant: "state"
        _transform:
            _value.double: |
                {
                    if (temp("state") == "active") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
    -
        _temp:
            localstate:
                _text: "${root}/group/local-info/state"
            peerstate:
                _text: "${root}/group/peer-info/state"
        _tags:
            "im.name":
                _constant: "cluster-state"
            "name":
                _constant: "Firewall Clustering"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Cluster State"
            "im.dstype.displayType":
                _constant: "state"
        _transform:
            _value.double: |
                {
                    if (temp("localstate") != "down" && temp("peerstate") != "down" && temp("peerstate") != "unknown") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
    -
        _temp:
            runningsync:
                _text: "${root}/group/running-sync"
        _tags:
            "im.name":
                _constant: "cluster-config-synced"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Cluster Configuration Synced"
            "im.dstype.displayType":
                _constant: "boolean"
        _transform:
            _value.double: |
                {
                    if (temp("runningsync") == "synchronized") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
    -
        _temp:
            preemptive:
                _text: "${root}/group/local-info/preemptive"
        _tags:
            "im.name":
                _constant: "cluster-preemption-enabled"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Preemption Enabled"
            "im.dstype.displayType":
                _constant: "boolean"
        _transform:
            _value.double: |
                {
                    if (temp("preemptive") == "yes") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
