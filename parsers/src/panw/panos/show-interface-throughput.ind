#! META
name: panos-show-interface-throughput
description: fetch interface throughput to calculate bandwidth used
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS

network-interface-speed:
    why: |
        Generally, these days network interfaces are set at 1Gbps or more. Sometimes, due to a configuration or device issue, an interface can be set below that (to 100mbps or even 10mbps). As that is usually _not_ the intended behavior, it is important to track the speed of all network interfaces.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the actual runtime speed of each interface.
    without-indeni: |
        The status of network interfaces is visible through the web interface. If a configured interface is down, it will appear with a red indicator.
    can-with-snmp: true
    can-with-syslog: true


network-interface-tx-bytes:
    why: |
        Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of bytes transmitted through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be manually reviewed through the CLI.
    can-with-snmp: true
    can-with-syslog: true

network-interface-rx-bytes:
    why: |
        Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of bytes received through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be manually reviewed through the CLI.
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::SSH
show interface all
show counter interface all 
ping count 11 host 127.0.0.1
show counter interface all 

#! PARSER::AWK

##################################
#
#	Determine interface name
#	
##################################

#Interface: ethernet1/1
/^Interface: / {
    interfaceName = $2
   
}

##################################
#
#	Record bytes sent/received
#	
##################################

#rx-bytes                      1122967921892
#tx-bytes                      161473516961
/^rx-bytes/ {
	bytesRx = $2
	
	
	
	if (interfaceName in bytesRxArr) { # if the array bytesRxArr already contains data for the interface
		# Calculate diff
		# How many bytes sent first run - how many bytes sent second run
		# Divided by 10 - To get into per second as first and second run was 10 seconds apart
		# Times 8 - convert from byte to bit
		# Divided into 1000 - Get into kilobit
		bytesRxArr[interfaceName] = (((bytesRx - bytesRxArr[interfaceName]) / 10) * 8) / 1000 # Result kilobit per second
		
	} else {
		bytesRxArr[interfaceName] = bytesRx
		
	}
	
}
/^tx-bytes/ {
         bytesTx = $2
	
		
	if (interfaceName in bytesTxArr) { # if the array bytesTxArr already contains data for the interface
		# Calculate diff
		# How many bytes sent first run - how many bytes sent second run
		# Divided by 10 - To get into per second as first and second run was 10 seconds apart
		# Times 8 - convert from byte to bit
		# Divided into 1000 - Get into kilobit
		bytesTxArr[interfaceName] = (((bytesTx - bytesTxArr[interfaceName]) / 10) * 8) / 1000  # Result kilobit per second
	} else {
		bytesTxArr[interfaceName] = bytesTx
	}

}
##################################
#
#	Determine speed of interface
#	
##################################

#ethernet1/1             16    100/full/up  
/\/full\/up\s+/ {

	speedData = $3
	interfaceName = $1
	
	# Remove prefix and b/s
	 gsub(/[A-Za-z]+|\//, "", speedData)
	
	
	speedKbitArr[interfaceName] = speedData *1000
	
}

END {

	##################################
	#
	#	Remove interfaces that do not 
	#	have a known speed
	#	
	##################################
	for (interface in bytesTxArr) {
		if (!(interface in speedKbitArr)) {
			delete bytesTxArr[interface]
		}	
	}
	
	for (interface in bytesRxArr) {
		if (!(interface in speedKbitArr)) {
			delete bytesRxArr[interface]
		}	
	}
	
	##################################
	#
	#	Calculate percentage interface 
	#	usage and write metric data
	#	
	##################################	
       
	for (interface in bytesTxArr) {
		interfaceTags["name"] = interface
		percentageTxUsed[interface] = (bytesTxArr[interface] / speedKbitArr[interface]) * 100
		writeDoubleMetricWithLiveConfig("network-interface-tx-util-percentage", interfaceTags, "gauge", "60", percentageTxUsed[interface], "Network Interfaces - Throughput Transmit", "percentage", "name")
		
	}
	
	for (interface in bytesRxArr) {
		interfaceTags["name"] = interface
		percentageRxUsed[interface] = (bytesRxArr[interface] / speedKbitArr[interface]) * 100
		writeDoubleMetricWithLiveConfig("network-interface-rx-util-percentage", interfaceTags, "gauge", "60", percentageRxUsed[interface], "Network Interfaces - Throughput Receive", "percentage", "name")
		
	}
}