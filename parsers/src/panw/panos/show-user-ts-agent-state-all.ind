#! META
name: panos-show-user-ts-agent-state
description: get the status of the terminal services agents
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
ts_agent_state:
    why: |
        The User-ID feature can be used in conjuction with Terminal Services agents ( https://live.paloaltonetworks.com/t5/Configuration-Articles/How-to-Install-and-Configure-Terminal-Server-Agent/ta-p/53839 ) to collect information pertaining to which user is logged a session on a terminal services server. If the communication with an agent is down, the Palo Alto Networks firewall won't know which users are logged into each device and may be block access to critical resources (if the Terminal Services agent is used to limit access to them).
    how: |
        This alert uses the Palo Alto Networks API to retrieve the current status of the Terminal Services agents (the equivalent of running "show user ts-agent state all" in CLI).
    without-indeni: |
        An administrator can log into the web interface to determine the status of the agents. Normally, this would be a result of an outage reported, pertaining to certain users' access to network resources.
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><user><ts-agent><state>all</state></ts-agent></user></show>&key=${api-key}
protocol: HTTPS

#! PARSER::AWK
BEGIN {
}

# Agent: Some USERID(vsys: vsys1) Host: 1.1.1.1:222
/^\s*Agent/ {
	line=$0
	sub(/^\s*Agent:\s*/, "", line)
	agentname=line
}

# 	Status                                            : not-conn:idle(Error: Failed to connect to User-ID-Agent at 1.1.1.1(1.1.1.1):222
# OR
# 	Status                                            : conn
/^\sStatus/ {
	if ($NF == "conn") {
		state = 1
	} else {
		state = 0
	}
	
	agenttags["name"] = agentname
	writeDoubleMetricWithLiveConfig("ts_agent_state", agenttags, "gauge", 60, state, "Terminal Services Agents", "state", "name")
}
END {
}
