#! META
name: panos-show-admins
description: fetch the list of logged in admin users
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos

#! COMMENTS
logged-in-users:
    why: |
        An idle user logged into a device is not a good security practice. This is especially true if the user logged into the device physically (from a console), as anyone passing by in the data center could access the device. For example, consider this document: http://www.security-solutions.co.za/cisco-asa-firewall-hardening-cisco-asa-best-practices.html and the requirement to _"Enforce an idle timeout to detect and close inactive sessions"_.
    how: |
        This alert uses the Palo Alto Networks API to retrieve the list of logged in admins, their source and their idle time.
    without-indeni: |
        An administrator could write a script to leverage the Palo Alto Networks API or could periodically log into the web interface and see the list of logged in users.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><admins><%2Fadmins><%2Fshow>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result
_metrics:
    -
        _groups:
            ${root}/admins/entry:
                _temp:
                    idlefor: 
                        _text: "idle-for"
                _tags:
                    "im.name":
                        _constant: "logged-in-users"
                _value.complex:
                    "from": 
                        _text: "from"
                    "username":
                        _text: "admin"
        _transform:
            _value.complex:
                "idle": |
                    {
                        str = temp("idlefor")
                        sub(/s/, "", str)
                        split(str, timearr, ":")
                        print timearr[1] * 3600 + timearr[2] * 60 + timearr[3]
                    }
        _value: complex-array
