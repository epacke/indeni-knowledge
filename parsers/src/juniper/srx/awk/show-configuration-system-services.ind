#! META
name: junos-show-configuration-system-services
description: identify whether telnet and http services are enabled 
type: monitoring
monitoring_interval: 10 minute
requires:
    vendor: juniper
    os.name: junos

#! COMMENTS
telnet-enabled:
http-server-enabled:
    why: |
        The system services "telnet" and "http" are not recommanded to enable on the device for security reasons.
    how: |
        If "telnet" and "http" are enabled on the device, it is recommanded to disable them and enable "ssh" and "https" instead to remediate the security risks.
    without-indeni: |
        An administrator could log on to the device to identify whether telnet and http are enabled.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show configuration system services | display set 

#! PARSER::AWK
BEGIN{
    telnet_enabled = "false"
    telnet_deactivated = 0
    http_enabled = "false"
    http_deactivated = 0
}

#set system services telnet
#deactivate system services telnet
/^(set|deactivate)(\s+system\s+services\s+telnet)/ { 
    telnet_service = $1
    if (telnet_service == "deactivate") {
        telnet_enabled = "false"
        telnet_deactivated = 1
    } else if (telnet_deactivated == 0) {
        telnet_enabled = "true"
    }
}

#set system services web-management http interface vlan.0
#deactivate system services web-management http interface vlan.0 
/^(set|deactivate)(\s+system\s+services\s+web-management\s+http)/ {
    http_service = $1
    if (http_service == "deactivate") {
        http_enabled = "false"
        http_deactivated = 1
    } else if (http_deactivated == 0) {
        http_enabled = "true"
    }
}


END {
   writeComplexMetricString("telnet-enabled", null, telnet_enabled)
   writeComplexMetricString("http-server-enabled", null, http_enabled)
}
