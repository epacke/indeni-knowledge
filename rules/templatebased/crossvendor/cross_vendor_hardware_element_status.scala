package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_hardware_element_status(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_hardware_element_status",
  ruleFriendlyName = "All Devices: Hardware element down",
  ruleDescription = "Alert if any hardware elements are not operating correctly.",
  metricName = "hardware-element-status",
  applicableMetricTag = "name",
  alertItemsHeader = "Hardware Elements Affected",
  alertDescription = "The hardware elements listed below are not operating correctly.",
  baseRemediationText = "Troubleshoot the hardware element as soon as possible.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Use the "show environment [ fan | power | temperature ]" NX-OS command to display information about the hardware environment status.
      |2. For more information please review the following CISCO Nexus HW Troubleshooting (fan/PS/Temp/Xbar/SUP) guide: https://www.cisco.com/c/en/us/support/docs/switches/nexus-7000-series-switches/200148-Troubleshooting-N7K-HW-fan-PS-Temp-Xbar.html
      |
      |In case of issue to the transceiver:
      |1. Use the "show interface transceiver detailed" NX-OS command to display detailed information for transceiver interfaces.
      |2. Use the "show interface transceiver calibrations" NX-OS  command to display calibration information for transceiver interfaces""".stripMargin
)
