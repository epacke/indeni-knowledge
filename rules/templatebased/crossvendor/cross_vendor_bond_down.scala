package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, StateDownTemplateRule}

/**
  *
  */
case class cross_vendor_bond_down(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_bond_down",
  ruleFriendlyName = "All Devices: Bond/LACP interface down",
  ruleDescription = "indeni will alert if a bond interface is down.",
  metricName = "bond-state",
  applicableMetricTag = "name",
  alertItemsHeader = "Interfaces Affected",
  descriptionStringFormat = "",
  alertDescription = "One or more bond interfaces are down.",
  baseRemediationText = "Review the cause for the interfaces being down.")(
  ConditionalRemediationSteps.VENDOR_CP -> "Use the \"cphaconf show_bond\" command to get additional information.",
  ConditionalRemediationSteps.VENDOR_PANOS -> "Use the \"show lacp\" command to get additional information."
)
