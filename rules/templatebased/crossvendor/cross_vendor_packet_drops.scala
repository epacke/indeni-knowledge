package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NumericThresholdOnDoubleMetricWithItemsTemplateRule

/**
  *
  */
case class cross_vendor_packet_drops(context: RuleContext) extends NumericThresholdOnDoubleMetricWithItemsTemplateRule(context,
  ruleName = "cross_vendor_packet_drops",
  ruleFriendlyName = "All Devices: Packet drop counters increasing",
  ruleDescription = "indeni will track packet drop counters and alert if any important counters are incrementing.",
  metricName = "packet-drop-counter",
  applicableMetricTag = "name",
  threshold = 100.0,
  alertDescription = "Some devices track the number of packets being dropped for various reasons. The current packet drop counters which are indicating dropped packets are listed below.",
  alertItemDescriptionFormat = "The drop counter is increasing at a rate of %.0f per second.",
  baseRemediationText = "Contact your technical support provider.",
  alertItemsHeader = "Affected Counters",
  itemsToIgnore = Set("flow_tcp_non_syn_drop".r, "flow_fwd_l3_bcast_drop".r, "flow_host_service_deny".r, "flow_ipv6_disabled".r, "flow_rcv_dot1q_tag_err".r, "flow_parse_l4_tcpsynfin".r, "flow_parse_l4_tcpfin".r, "flow_fwd_l3_mcast_drop".r, "^$".r))(
)
