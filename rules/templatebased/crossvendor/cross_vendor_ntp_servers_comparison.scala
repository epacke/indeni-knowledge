package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_ntp_servers_comparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_ntp_servers_comparison",
  ruleFriendlyName = "Clustered Devices: NTP servers used do not match across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the NTP servers they are using are different.",
  metricName = "ntp-servers",
  isArray = true,
  alertDescription = "Devices that are part of a cluster must have the same NTP servers used. Review the differences below.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/itzik-assaraf/2/870/1b5\">Itzik Assaraf</a> (Leumi Card).",
  baseRemediationText = "Review the NTP configuration on each device to ensure they match.")()
