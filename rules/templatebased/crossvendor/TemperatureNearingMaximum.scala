package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class TemperatureNearingMaximum(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "TemperatureNearingMaximum",
  ruleFriendlyName = "All Devices: Temperature nearing maximum",
  ruleDescription = "The temperature reported by the device is nearing the maximum allowed. If any of the temperatures is nearing its maximum limit, an alert will be issued.",
  usageMetricName = "temperature-sensor-current",
  limitMetricName = "temperature-sensor-max",
  applicableMetricTag = "name",
  threshold = 90.0,
  alertDescription = The temperature reported by the device is nearing the maximum allowed. If any of the temperatures is nearing its maximum limit, an alert will be issued. If not addressed in a timely manner, permanent damage can result with initial symptoms being that of poor performance and loss or corruption of data. Firewalls can overheat due to several different factors. Some examples include a broken fan, undersized CPU, poor circulation or high temperatures in the room it is stored.",
  alertItemDescriptionFormat = "Sensor: %.0f where the maximum limit is %.0f.",
  baseRemediationText = "Check the hardware sensor values on the device and handle cause for high temperature",
  alertItemsHeader = "Device Sensors")(
  ConditionalRemediationSteps.VENDOR_PANOS ->
    """|
       | Run the command "show system environmentals" on the device to display the sensor values.
    """.stripMargin
)
