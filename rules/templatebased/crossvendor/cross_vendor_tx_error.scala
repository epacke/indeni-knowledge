package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class cross_vendor_tx_error(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_tx_error",
  ruleFriendlyName = "All Devices: TX packets experienced errors",
  ruleDescription = "Indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-tx-errors",
  limitMetricName = "network-interface-tx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of error packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high error rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f error packets identified out of a total of %.0f transmitted.",
  baseRemediationText = "Packet errors usually occur when there is a mismatch in the speed and duplex settings on two sides of a cable, or a damaged cable.",
  alertItemsHeader = "Affected Ports")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Run the "show interface" command to review the interface error counters and the bitrate. Consider to configure the "load-interval 30" interface sub command to improve the accuracy of the interface measurements.
      |2.Check for a mismatch in the speed and duplex interface settings on two sides of a cable, or for a damaged cable.
      |3. Use the "show interface counters errors" NX-OS command to display detailed interface error counters. If you do not specify an interface, this command displays information about all Layer 2 interfaces.""".stripMargin
)
