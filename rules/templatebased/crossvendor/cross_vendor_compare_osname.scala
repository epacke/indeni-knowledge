package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, SnapshotComparisonTemplateRule}

/**
  *
  */
case class cross_vendor_compare_osname(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_osname",
  ruleFriendlyName = "Clustered Devices: OS name mismatch across cluster members",
  ruleDescription = "Indeni will identify when two devices are part of a cluster and alert if the OS installed is different.",
  metricName = "os-name",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same OS's installed.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"http://il.linkedin.com/pub/gal-vitenberg/83/484/103\">Gal Vitenberg</a>.",
  baseRemediationText = "Install the correct versions of software on each device.")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Check that the vPC peers have the same NX-OS version except during the non-disruptive upgrade, that is, In-Service Software Upgrade (ISSU).
      |2. Execute the "show version" NX-OS command and check the installed NX-OS version across the vPC peer switches.
      |3. Schedule a Maintenance Window for NX-OS upgrade in order the vPC peer switches have exact the same NX-OS version.
      |NOTE: The vPC could be established between the vPC peers with not exact the same NX-OS name but several problems will be faced when new features are configured. For instance FEX-mismatch SW log message will be generated if you try to connect a FEX via vPC to a pair of vPC switches with different SW version. In this case the FEX will be operational only from one of the vPC peer switches.""".stripMargin
)
