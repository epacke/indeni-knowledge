package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_radius_servers_comparison(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_radius_servers_comparison",
  ruleFriendlyName = "Clustered Devices: RADIUS servers used do not match across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the RADIUS servers they are using are different.",
  metricName = "radius-servers",
  isArray = true,
  alertDescription = "Devices that are part of a cluster should have the same RADIUS servers configured. Review the differences below.",
  baseRemediationText = "Review the RADIUS configuration on each device to ensure they match.")()
