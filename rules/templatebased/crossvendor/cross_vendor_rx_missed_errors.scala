package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.CounterIncreaseTemplateRule

/**
  *
  */
case class cross_vendor_rx_missed_errors(context: RuleContext) extends CounterIncreaseTemplateRule(context,
  ruleName = "cross_vendor_rx_missed_errors",
  ruleFriendlyName = "All Devices: rx_missed_errors increasing",
  ruleDescription = "indeni watches many important port/NIC specific counters. The rx_missed_errors is one of them.",
  metricName = "network-interface-rx-missed-errors",
  applicableMetricTag = "name",
  alertDescription = "Some interfaces are experiencing rx_missed_errors.",
  alertRemediationSteps = "Read http://serverfault.com/questions/702555/how-to-troubleshoot-rx-missed-errors .",
  alertItemsHeader = "Affected Interfaces"
)()
