package com.indeni.server.rules.library.templatebased.paloaltonetworks

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityWithItemsTemplateRule

/**
  *
  */
case class palo_alto_networks_high_logdb_usage(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "palo_alto_networks_high_logdb_usage",
  ruleFriendlyName = "Palo Alto Networks Firewalls: High log DB usage",
  ruleDescription = "indeni will alert if the log DB utilization of a device is above a high threshold.",
  usageMetricName = "logdb-usage",
  applicableMetricTag = "name",
  threshold = 95.0,
  alertDescription = "Some log DBs are nearing their quota. The device will automatically purge old logs (per https://live.paloaltonetworks.com/t5/Management-Articles/When-are-Logs-Purged-on-the-Palo-Alto-Networks-Devices/ta-p/53605). This may be a critical issue if these logs are not retained and should be.",
  alertItemDescriptionFormat = "Current log DB utilization is: %.0f%%",
  baseRemediationText = "More information is available at https://live.paloaltonetworks.com/t5/Management-Articles/How-to-Determine-How-Much-Disk-Space-is-Allocated-to-Logs/ta-p/53828",
  alertItemsHeader = "Log DBs Nearing Quota")()
