package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class checkpoint_compare_pbr_rules(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "checkpoint_compare_pbr_rules",
  ruleFriendlyName = "Check Point Cluster: PBR rules mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the PBR rules settings are different.",
  metricName = "pbr-rules",
  isArray = true,
  alertDescription = "The members of a cluster of Check Point firewalls must have the same PBR (policy based routing) settings.",
  baseRemediationText = """Compare the output of "show pbr rules" (under clish) across members of the cluster.""")()
