package com.indeni.server.rules.library.templatebased.radware

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class radware_dynamic_store_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "radware_dynamic_store_limit",
  ruleFriendlyName = "Radware Alteon: Dynamic data store limit nearing",
  ruleDescription = "The dynamic data store is used by the AppShape++ feature. indeni will alert prior to the data store reaching its limit.",
  usageMetricName = "dynamic-data-store-usage",
  limitMetricName = "dynamic-data-store-limit",
  threshold = 80.0,
  alertDescriptionFormat = "The dynamic data store usage is %.0f where the limit is %.0f.",
  baseRemediationText = "Review https://kb.radware.com/Questions/Alteon/Public/ALERT-slb-Dynamic-Data-Store-is-at-FULL-capacity")()
