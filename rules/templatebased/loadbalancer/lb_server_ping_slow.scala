package com.indeni.server.rules.library.templatebased.loadbalancer

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class lb_server_ping_slow(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "lb_server_ping_slow",
  ruleFriendlyName = "Load Balancers: Pool member / server response time high",
  ruleDescription = "indeni will alert if the ping from the load balancer to specific servers is too high.",
  usageMetricName = "lb-server-ping-response",
  threshold = 20.0,
  applicableMetricTag = "name",
  alertItemsHeader = "Slow Servers/Pool Members",
  alertDescription = "Some pool members' or servers' response time is too high.",
  alertItemDescriptionFormat = "The response time for this pool member or server is %.0f ms",
  baseRemediationText = "Review any networking or communication issues which may cause this.")(
  ConditionalRemediationSteps.VENDOR_F5 -> "Try running the following command on the device under bash:\ntmsh -q -c 'cd /;list ltm node recursive' | awk '/^ltm node/{print \"Node: \" $3} /address/{ system(\"ping -c1 \" $2)};'"
)
