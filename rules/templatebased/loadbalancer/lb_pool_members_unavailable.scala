package com.indeni.server.rules.library.templatebased.loadbalancer

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class lb_pool_members_unavailable(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "lb_pool_members_unavailable",
  ruleFriendlyName = "Load Balancers: Pool member(s) unavailable",
  ruleDescription = "indeni will alert if a pool member which should be available is not.",
  metricName = "lb-pool-member-state",
  applicableMetricTag = "name",
  descriptionMetricTag = "pool-name",
  alertItemsHeader = "Pool Members Affected",
  alertDescription = "Certain pool members which should available are not. Review list below.",
  baseRemediationText = "Determine why the members are down and resolve the issue as soon as possible.")()
