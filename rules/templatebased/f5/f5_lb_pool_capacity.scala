package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{NearingCapacityWithItemsTemplateRule, ThresholdDirection}

/**
  *
  */
case class f5_lb_pool_capacity(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "f5_lb_pool_capacity",
  ruleFriendlyName = "F5 Devices: Pools operating at a low capacity",
  ruleDescription = "indeni will alert if the the number of members available in the pool is too low, based on the percentage of members available out of the total.",
  usageMetricName = "lb-pool-capacity",
  threshold = 50.0,
  thresholdDirection = ThresholdDirection.BELOW,
  applicableMetricTag = "name",
  alertItemsHeader = "Affected Pools",
  alertDescription = "The pools listed below have members that are not able to process traffic due to either failed monitors or members being disabled. This means that the pools may not be able to handle the traffic load they are meant to.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  alertItemDescriptionFormat = "Only %.0f%% of pool members are available.",
  baseRemediationText = "Log into the device and examine the status of the pool. Troubleshoot the application in case of failed monitors and verify that any disabled members is intentional.\nTroubleshooting steps for HTTP monitors:\n1. Log into the device through SSH.\n2. Issue the following command: echo -ne \"<monitor send string>\" | nc <member ip> <member port>.\n3. Make sure you get a response and that the response matches any receive string you have configured.\nExample:\necho -ne \"GET / HTTP/1.1\\r\\nHost:myapplication.domain.local\\r\\nUser-agent: Mozilla/5.0 (Windows NT 6.1\\r\\n\\r\\n\" | nc 10.10.10.1 8080\nTroubleshooting steps for HTTPS monitors:\n1. Log into the device through SSH.\n2. Issue the following command: curl -k https://<member ip>:<member port><URI part of the monitor send string>\n3. Make sure you get a response and that the response matches any receive string you have configured.\nNote: You might have to add additional header specified in the send string using --header.\nExample:\ncurl -vvv --header \"Host:myapplication.domain.local\" --header \"User-agent:Mozilla/5.0 (Windows NT 6.1\" https://10.10.10.1:443/\nTroubleshooting steps for TCP monitors with send strings:\n1. Log into the device through SSH\n2. Issue the following command: echo -ne \"<monitor send string>\" | nc <member ip> <member port>\n3. Make sure you get a response and that the response matches any receive string you have configured.\nExample:\necho -ne \"info\\r\\nquit\\r\\n\" | nc 10.10.10.1 8080\nTroubleshooting steps for TCP monitors without send strings:\n1. Log into the device through SSH\n2. Issue the following command: telnet <member ip> <member port>\nExample\ntelnet 10.10.10.1 8080")()
